export function setState(state) {
    let response;
    switch (state) {
        case "ENABLED":
            response = "Habilitado";
            break;
        case "DISABLED":
            response = "Deshabilitado";
            break;
    }
    return response;
}

export function setRole(state) {
    let response;
    switch (state) {
        case "ADMIN":
            response = "Administrador";
            break;
        case "VENDOR":
            response = "Vendedor";
            break;
        case "SUPERVISOR":
            response = "Supervisor";
            break;
        case "MASTER":
            response = "Master";
            break;
    }
    return response;
}

export function setDate(date) {
    let dateFormatted = "";
    if (date != null) {
        dateFormatted = new Date(date).toLocaleString();
    }
    return dateFormatted;
}

export function toLocaleString(date) {
    const d = new Date(date);

    const options = {
        year: "numeric",
        month: "2-digit",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit"
    };

    return d.toLocaleString("zu", options);
}

export function formatDate(date) {
    let d = new Date(date),
        month = `${d.getMonth() + 1}`,
        day = `${d.getDate()}`,
        year = d.getFullYear();

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [year, month, day].join("-");
}

export function dateWithoutGMT(date) {
    return date.replace(" GMT-05:00", "");
}

export function secondsToHHmmss(totalSeconds) {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds - hours * 3600) / 60);
    let seconds = totalSeconds - hours * 3600 - minutes * 60;

    seconds = Math.round(seconds * 100) / 100;
    const response = `${hours}h ${minutes}m ${seconds}s`;
    return response;
}

export function minutesToSeconds(minutes) {
    const seconds = Math.floor(minutes * 60);
    return seconds;
}

export function setVehicleType(vehicleType) {
    switch (vehicleType) {
        case "CAR":
            return "Carro";
        case "MOTORCYCLE":
            return "Moto";
        case "BICYCLE":
            return "Bicicleta";
        case "TRUCK":
            return "Camión";
        case "PUBLIC":
            return "Servicio Público";
        default:
            return "Otro";
    }
}

export function setPaymentType(vehicleType) {
    switch (vehicleType) {
        case "NORMAL":
            return "Normal";
        case "AGREEMENT":
            return "Convenio";
        case "SUBSCRIPTION":
            return "Suscripción";
        default:
            return "Otro";
    }
}

export function setTime(time) {
    switch (time) {
        case "DAY":
            return "Día";
        case "MONTH":
            return "Mes";
        case "TRIMESTER":
            return "Trimestre";
        case "SEMESTER":
            return "Semestre";
        case "YEAR":
            return "Año";
        default:
            return "Otro";
    }
}

export function textNullToNull(text) {
    if (text === "null") {
        return null;
    }
    return text;
}

export function changeNullToEmpty(data) {
    if (data == null) {
        return "";
    }
    return data;
}

export function doPdf(title, additionalInformation, landscape) {
    const docDefinition = {
        pageOrientation: landscape,
        footer: {
            text: "www.ideamosmas.com",
            alignment: "center"
        },
        content: [
            {
                text: title,
                style: "header"
            },
            {
                text: "Numbered list example:"
            },
            {
                text: additionalInformation,
                style: "header"
            }
        ],
        styles: {
            header: {
                fontSize: 22,
                bold: true,
                alignment: "center",
                margin: [5, 2, 10, 20]
            }
        }
    };

    pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
    pdfMake.createPdf(docDefinition).open();
}

export function ticketToPdf(title, data, landscape) {
    const docDefinition = {
        pageOrientation: landscape,
        pageSize: {
            width: 150,
            height: 600
        },
        content: data,
        pageMargins: [5, 5, 5, 5]
    };
    pdfMake.createPdf(docDefinition).print();
}

export function convertTableToPdf(title, data, additionalInformation, landscape) {
    const docDefinition = {
        pageOrientation: landscape,
        footer: {
            text: "www.ideamosmas.com",
            alignment: "center"
        },
        content: [
            {
                text: title,
                style: "header"
            },
            {
                table: {
                    body: data
                }
            },
            {
                text: additionalInformation,
                style: "header"
            }
        ],
        styles: {
            header: {
                fontSize: 22,
                bold: true,
                alignment: "center",
                margin: [5, 2, 10, 20]
            }
        }
    };

    pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
    pdfMake.createPdf(docDefinition).open();
}

export function convertTableToXls(title, columns, data) {
    const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
            title
        },
        style: "background:#ffffff",
        column: {
            style: "font-size:20px"
        },
        columns,
        row: {
            style(sheet, row, rowidx) {
                return `background:${rowidx % 2 ? "white" : "white"}`;
            }
        },
        rows: {
            4: {
                cell: {
                    style: "background:white"
                }
            }
        }
    };
    alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, data]);
}

const timepicker = {
    defaultTime: "now",
    fromNow: 0,
    twelveHour: false,
    i18n: {
        done: "OK",
        clear: "Limpiar",
        cancel: "Cancelar"
    },
    autoClose: true,
    ampmclickable: true,
    aftershow() { }
};

const datepicker = {
    i18n: {
        months: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        monthsShort: [
            "Ene",
            "Feb",
            "Mar",
            "Abr",
            "May",
            "Jun",
            "Jul",
            "Ago",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ],
        weekdays: [
            "Domingo",
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sabado"
        ],
        weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"],
        cancel: "Cancelar",
        today: "Hoy",
        done: "Ok"
    },
    format: "yyyy-mm-dd",
    clear: "Limpiar",
    formatSubmit: "yyyy-mm-dd",
    hiddenPrefix: "prefix__",
    hiddenSuffix: "__suffix",
    showMonthsShort: true,
    selectMonths: true,
    autoClose: true,
    showDaysInNextAndPreviousMonths: true,
    setDefaultDate: true,
    defaultDate: new Date()
};

export function daysBetween(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}

export function setSuccessMessage(data) {
    const toastContent = `<i class="material-icons">info</i><span style="color:#ffffff; margin-left:10px;">${data}</span>`;
    M.toast({
        html: toastContent,
        classes: "light-green darken-3",
        displayLength: 5000
    });
}

export function setInformativeMessage(data) {
    const toastContent = `<i class="material-icons">info</i><span style="color:#ffffff; margin-left:10px;">${data}</span>`;
    M.toast({
        html: toastContent,
        classes: "blue darken-1",
        displayLength: 5000
    });
}

export function setWarningMessage(data) {
    const toastContent = `<i class="material-icons">info</i><span style="color:#ffffff; margin-left:10px;">${data}</span>`;
    M.toast({
        html: toastContent,
        classes: "amber darken-1",
        displayLength: 5000
    });
}
