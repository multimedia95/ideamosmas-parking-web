// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'https://parking-backend-api.azurewebsites.net/api/',
  nodeUrl: 'http://192.168.0.184:3977/api/',
  urlInit: 'https://parking-backend-admin.azurewebsites.net/api/',
  // urlInit = 'http://localhost:8081/api/',
  reportUrl: 'https://parking-backend-report.azurewebsites.net/api/',
  // reportUrlDW : "http://localhost:3000/api/",
  pruebaPassword: 'https://parkingmobile-admin.azurewebsites.net/api/',
  paymentApi: 'https://prueba-payment.azurewebsites.net/api/',
  apiBlackListUrl: 'http://dev-blacklist.azurewebsites.net/api/',
  apiPasword: 'RRDT7TC-TXAMKF6-GATPXMS-JQQ5SSY'
};
