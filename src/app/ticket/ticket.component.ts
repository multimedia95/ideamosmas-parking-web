import { Component, OnInit } from '@angular/core';
import { TicketService } from '../service/ticket.service'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as Utils from '../../assets/utils/utils';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  public vehiclePlate: string;
  public showLoading: boolean;

  constructor(public ticketService: TicketService) {
  }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.vehiclePlate = '';
    this.showLoading = false;
  }

  public printEntranceTicket(operation) {
    const data = [];
    data.push([
      {
        text: operation.companyName,
        alignment: "center"
      },
      {
        text: `NIT: ${operation.companyTaxIdentification}`,
        alignment: "center"
      },
      {
        text: `Dirección: ${operation.parkingAddress}`,
        alignment: "center"
      },
      {
        text: operation.parkingEmail,
        alignment: "center"
      },
      {
        text: `Teléfono: ${operation.parkingPhone}`,
        alignment: "center",
        margin: [0, 0, 0, 10]
      },
      {
        text: `Fecha: ${operation.created}`,
        alignment: "left"
      },
      {
        text: `Matrícula: ${operation.vehiclePlate}`,
        alignment: "left"
      },
      {
        text: `Tipo de Vehiculo: ${Utils.setVehicleType(operation.vehicleType)}`,
        alignment: "left"
      },
      {
        text: `Operario: ${operation.userName}`,
        alignment: "left"
      },
      {
        qr: operation.operationCode,
        alignment: "center",
        margin: [0, 10, 0, 10]
      },
      {
        text: operation.subscriptionInformation === null ? '' : operation.subscriptionInformation,
        alignment: "center",
        margin: [0, 10, 0, 0]
      },
      {
        text: operation.parkingAdditionalInformation === null ? '' : operation.parkingAdditionalInformation,
        alignment: "center"
      }
    ]);
    const docDefinition = {
      pageOrientation: '',
      pageSize: {
        width: 150,
        height: 600
      },
      content: data,
      pageMargins: [5, 5, 5, 5]
    };
    pdfMake.createPdf(docDefinition).print();
  }

  public searchTicket() {
    this.showLoading = true;
    const data = {
      vehiclePlate: this.vehiclePlate,
      parkingId: sessionStorage.getItem('parkingId')
    }
    this.ticketService.findEntranceOperation(data).subscribe(data => {
      this.showLoading = false;
      this.printEntranceTicket(data);
    }, error => {
      this.showLoading = false;
      console.log("No se encontró un vehículo con dicha matrícula");
    })
  }

}
