import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { SubscriptionService } from 'app/service/subscription.service'
import { ToastService } from 'app/service/toast-service/toast-service'

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}


declare interface SubscriptionPrice {
  id: string;
  subscriptionTime: string;
  cost: number;
  tax: number;
  vehicleType: string;
  name: string;
}

@Component({
  selector: 'app-subscription-fare',
  templateUrl: './subscription-fare.component.html',
  styleUrls: ['./subscription-fare.component.scss']
})
export class SubscriptionFareComponent implements OnInit {

  showLoading: boolean;
  showInformationNotFound: boolean;
  subscriptionPrices: SubscriptionPrice[];
  subscriptionPricesToDisplay: SubscriptionPrice[];
  onEdit: boolean = false;
  newSubscriptionPrice: SubscriptionPrice = { tax: 0, subscriptionTime: '', id: '', cost: 0, vehicleType: '', name: '' }
  lastSort: SortEvent;
  icons = [
    {
      name: 'CAR',
      icon: 'directions_car'
    },
    {
      name: 'MOTORCYCLE',
      icon: 'motorcycle'
    },
    {
      name: 'BICYCLE',
      icon: 'directions_bike'
    },
    {
      name: 'PUBLIC',
      icon: 'local_taxi'
    },
    {
      name: 'TRUCK',
      icon: 'local_shipping'
    }
  ]
  timeList = [
    {
      name: "Día",
      value: "DAY"
    },
    {
      name: "Mes",
      value: "MONTH"
    },
    {
      name: "Trimestre",
      value: "TRIMESTER"
    },
    {
      name: "Semestre",
      value: "SEMESTER"
    },
    {
      name: "Año",
      value: "YEAR"
    }
  ];
  vehicleTypeList = [
    {
      name: 'Carro',
      value: 'CAR'
    },
    {
      name: 'Moto',
      value: 'MOTORCYCLE'
    },
    {
      name: 'Bicicleta',
      value: 'BICYCLE'
    },
    {
      name: 'Servicio Público',
      value: 'PUBLIC'
    },
    {
      name: 'Camión',
      value: 'TRUCK'
    }
  ]

  page = 1;
  pageSize = 10;

  constructor(
    private subscriptionService: SubscriptionService,
    private toastService: ToastService
  ) { }

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  async ngOnInit() {
    this.lastSort = { column: '', direction: '' }
    this.showLoading = false;
    this.showInformationNotFound = false;
    await this.getSubscriptionPrices();
    this.onSort(this.lastSort);

  }

  onFilter(textToFilter: string) {
    const text = textToFilter.toLocaleLowerCase();
    this.subscriptionPricesToDisplay = this.subscriptionPrices.filter(subscriptionPrice => {
      return (
        (subscriptionPrice.name ? subscriptionPrice.name.toString().toLowerCase().includes(text) : false)
        || (subscriptionPrice.subscriptionTime ? subscriptionPrice.subscriptionTime.toLowerCase().includes(text) : false)
        || (subscriptionPrice.vehicleType ? this.setVehicle(subscriptionPrice.vehicleType).toLowerCase().includes(text) : false)
        || (subscriptionPrice.cost ? subscriptionPrice.cost.toString().toLowerCase().includes(text) : false)
        || (subscriptionPrice.tax ? subscriptionPrice.tax.toString().toLowerCase().includes(text) : false)
      )
    })

    if (this.lastSort) {
      this.onSort(this.lastSort);
    }
  }

  onSort({ column, direction }: SortEvent) {

    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.lastSort = { column: column, direction: direction };

    if (direction === '') {
      // this.subscriptionsToDisplay = this.subscriptions;
    } else {
      this.subscriptionPricesToDisplay = [...this.subscriptionPricesToDisplay].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  async getSubscriptionPrices() {
    this.showInformationNotFound = false;
    this.showLoading = true;
    this.subscriptionService.findSubscriptionPrices(sessionStorage.getItem('companyId')).subscribe(data => {
      this.showLoading = false;
      this.subscriptionPrices = data.subscriptionPrices;
      this.subscriptionPricesToDisplay = data.subscriptionPrices;
    }, error => {
      this.showLoading = false;
      this.showInformationNotFound = true;
      console.log('No hay tarifas disponibles.');
      this.toastService.show('No hay tarifas disponibles.', { classname: 'bg-warning text-light' });
    })
  }

  saveSubscriptionPrice() {
    this.subscriptionService.createSubscriptionPrice(sessionStorage.getItem('companyId'), this.newSubscriptionPrice).subscribe(
      () => {
        this.toastService.show('Se creo la tarifa.', { classname: 'bg-success text-light' });
        this.getSubscriptionPrices();
        this.onCancel();
      },
      () => {
        this.toastService.show('Error al crear tarifa.', { classname: 'bg-danger text-light' });
      }
    )
  }

  onSelect(subscriptionPrice) {
    this.newSubscriptionPrice = subscriptionPrice;
    this.onEdit = true;
  }

  onEditSubscriptionPrice() {
    this.subscriptionService.updateSubscriptionPrice(sessionStorage.getItem('companyId'), this.newSubscriptionPrice.id, this.newSubscriptionPrice).subscribe(
      () => {
        this.toastService.show('Se editó la tarifa.', { classname: 'bg-success text-light' });
        this.getSubscriptionPrices();
        this.onCancel();
      },
      () => {
        this.toastService.show('Falló al editar tarifa.', { classname: 'bg-danger text-light' });
      }
    )
  }

  onCancel() {
    this.newSubscriptionPrice = { tax: 0, subscriptionTime: '', id: '', cost: 0, vehicleType: '', name: '' };
    this.onEdit = false;
  }

  setIcon(vehicleType) {
    if (this.icons.find(icon => icon.name === vehicleType)) {
      return this.icons.find(icon => icon.name === vehicleType).icon;
    } else {
      return 'directions_car'
    }
  }

  setTime(time) {
    if (this.timeList.find(t => t.value === time)) {
      return this.timeList.find(t => t.value === time).name;
    } else {
      return ''
    }
  }

  setVehicle(vehicle) {
    if (this.vehicleTypeList.find(v => v.value === vehicle)) {
      return this.vehicleTypeList.find(v => v.value === vehicle).name;
    } else {
      return ''
    }
  }

}