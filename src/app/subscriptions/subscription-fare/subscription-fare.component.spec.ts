import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionFareComponent } from './subscription-fare.component';

describe('SubscriptionFareComponent', () => {
  let component: SubscriptionFareComponent;
  let fixture: ComponentFixture<SubscriptionFareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionFareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionFareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
