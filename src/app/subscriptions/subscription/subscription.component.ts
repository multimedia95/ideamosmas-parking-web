import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { SubscriptionService } from 'app/service/subscription.service'
import { Subscription } from '../../domain/subscription';
import { CurrencyPipe } from '@angular/common';
import { DatePipe } from '@angular/common';
import { ToastService } from 'app/service/toast-service/toast-service'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}


declare interface SubscriptionPrice {
  id: string;
  subscriptionTime: string;
  cost: number;
  tax: number;
  vehicleType: string;
  name: string;
}

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {

  public subscriptions: Subscription[];
  public subscriptionsToDisplay: Subscription[];
  public showLoading: boolean;
  public showInformationNotFound: boolean;
  public subscriptionPrices: SubscriptionPrice[];
  public subscriptionSelected: Subscription;
  public newSubscription: Subscription;
  public subscriptionInvoice: Subscription;
  public moneyReceived: number;
  public searchEnded: boolean;
  public lastSort: SortEvent;

  page = 1;
  pageSize = 10;

  constructor(
    public subscriptionService: SubscriptionService,
    private currencyPipe: CurrencyPipe,
    private datePipe: DatePipe,
    private toastService: ToastService
  ) { }

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  async ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.lastSort = { column: 'created', direction: 'desc' }
    this.showLoading = false;
    this.showInformationNotFound = false;
    this.newSubscription = <Subscription>{};
    this.newSubscription.quantity = 1;
    this.getSubscriptionPrices();
    this.searchEnded = false;
    await this.getSubscriptions();
    // this.onSort(this.lastSort);

  }

  onFilter(textToFilter) {
    const text = textToFilter.toLocaleLowerCase();
    this.subscriptionsToDisplay = this.subscriptions.filter(subscription => {
      return (
        (subscription.created ? subscription.created.toString().toLowerCase().includes(text) : false)
        || (subscription.vehiclePlate ? subscription.vehiclePlate.toLowerCase().includes(text) : false)
        || (subscription.start ? subscription.start.toString().toLowerCase().includes(text) : false)
        || (subscription.end ? subscription.end.toString().toLowerCase().includes(text) : false)
        || (this.subscriptionPrices.find(sub => sub.id === subscription.subscriptionPriceId) ?
          this.subscriptionPrices.find(sub => sub.id === subscription.subscriptionPriceId).name.toLowerCase().includes(text) : false)
        || (subscription.quantity ? subscription.quantity.toString().toLowerCase().includes(text) : false)
        || (subscription.total ? subscription.total.toString().toLowerCase().includes(text) : false)
        || (subscription.username ? subscription.username.toLowerCase().includes(text) : false)
        || (subscription.subscriptionState ? subscription.subscriptionState.toLowerCase().includes(text) : false)
      )
    })

    if (this.lastSort) {
      this.onSort(this.lastSort);
    }
  }

  onSort({ column, direction }: SortEvent) {

    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.lastSort = { column: column, direction: direction };

    if (direction === '') {
      this.subscriptionsToDisplay = this.subscriptions;
    } else {
      this.subscriptionsToDisplay = [...this.subscriptionsToDisplay].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  onDateSelect(e) {
    this.newSubscription.start = new Date(e.year, (e.month - 1), e.day);
    this.calculateSubscription();
  }

  calculateSubscription() {
    if ((this.newSubscription.subscriptionPriceId !== undefined || this.newSubscription.subscriptionPriceId !== '') &&
      (this.newSubscription.quantity !== undefined || this.newSubscription.quantity > 0) &&
      (this.newSubscription.start !== undefined)) {
      this.subscriptionService.subscriptionInformation(
        sessionStorage.getItem('companyId'),
        sessionStorage.getItem('parkingId'),
        this.newSubscription
      ).subscribe(data => {
        this.newSubscription = data;
        this.newSubscription.start = new Date(this.newSubscription.start);
        this.newSubscription.end = new Date(this.newSubscription.end);
        this.searchEnded = true;
      }, error => {

      })
    }

  }

  async getSubscriptions() {
    this.showInformationNotFound = false;
    this.showLoading = true;
    this.subscriptionService.findSubscriptions(sessionStorage.getItem('parkingId')).subscribe(data => {
      this.showLoading = false;
      this.subscriptions = data.subscriptions;
      this.subscriptionsToDisplay = this.subscriptions;
    }, error => {
      this.showLoading = false;
      this.showInformationNotFound = true;
    })
  }

  getSubscriptionPrices() {
    this.subscriptionService.findSubscriptionPrices(sessionStorage.getItem('companyId')).subscribe(data => {
      this.subscriptionPrices = data.subscriptionPrices;
    }, error => {
      console.log('No hay tarifas disponibles.');
      this.toastService.show('No hay tarifas disponibles.', { classname: 'bg-warning text-light' });
    })
  }

  setSubscriptionPrice(priceId) {
    if (this.subscriptionPrices.find(price => price.id === priceId)) {
      return this.subscriptionPrices.find(price => price.id === priceId).name;
    } else {
      return '';
    }
  }

  disableSubscription(subscription: Subscription) {
    this.subscriptionSelected = subscription;
  }

  printSubscriptionCopy(subscriptionId) {
    this.subscriptionService.findSubscriptionById(
      sessionStorage.getItem('companyId'),
      sessionStorage.getItem('parkingId'),
      subscriptionId
    ).subscribe(data => {
      this.printSubscription(data);
    }, error => {
      console.log('No es posible imprimir la factura.');
      this.toastService.show('No es posible imprimir la factura.', { classname: 'bg-danger text-light' });
    })
  }

  printSubscription(subscription) {
    const data = [];
    data.push([
      {
        text: subscription.parkingName,
        alignment: 'center'
      },
      {
        text: `NIT: ${subscription.companyTaxIdentification}`,
        alignment: 'center'
      },
      {
        text: `No: ${subscription.taxResolutionRangePrefix}-${
          subscription.invoiceNumber
          }`,
        alignment: 'center'
      },
      {
        text: subscription.companyAddress,
        alignment: 'center'
      },
      {
        text: subscription.parkingEmail,
        alignment: 'center'
      },
      {
        text: `Teléfono: ${subscription.companyPhone}`,
        alignment: 'center',
        margin: [0, 0, 0, 10]
      },
      {
        text: `Fecha: ${this.datePipe.transform(subscription.created, 'y-MM-dd h:mm:ss a')}`,
        alignment: 'left'
      },
      {
        text: `Matrícula: ${subscription.vehiclePlate}`,
        alignment: 'left'
      },
      {
        text: `Tarjeta: ${subscription.additionalCode === null ? '' : subscription.additionalCode}`,
        alignment: 'left'
      },
      {
        text: `Operario: ${subscription.userName}`,
        alignment: 'left'
      },
      {
        text: `Desde:${this.datePipe.transform(subscription.start, 'y-MM-dd h:mm:ss a')}`,
        alignment: 'left'
      },
      {
        text: `Hasta: ${this.datePipe.transform(subscription.end, 'y-MM-dd h:mm:ss a')}`,
        alignment: 'left'
      },
      {
        text: `Tarifa: ${subscription.subscriptionPriceName}`,
        alignment: 'left'
      },
      {
        text: `Subtotal: ${this.currencyPipe.transform(subscription.amount, 'COP', '$')}`,
        alignment: 'left'
      },
      {
        text: `IVA: ${this.currencyPipe.transform(subscription.tax, 'COP', '$')}`,
        alignment: 'left'
      },
      {
        text: `TOTAL: ${this.currencyPipe.transform(subscription.total, 'COP', '$')}`,
        alignment: 'left',
        margin: [0, 0, 0, 10]
      },
      {
        text: `Número de Resolución: ${subscription.taxResolutionNumber} de ${
          subscription.taxResolutionDate
          }`,
        alignment: 'center'
      },
      {
        text: `Prefijo ${subscription.taxResolutionRangePrefix} Rango desde ${
          subscription.taxResolutionRangeFrom
          } hasta ${subscription.taxResolutionRangeTo}`,
        alignment: 'center',
        margin: [0, 0, 0, 10]
      },
      {
        text: '¡Gracias por su visita!',
        alignment: 'center'
      }
    ]);
    const docDefinition = {
      pageOrientation: '',
      pageSize: {
        width: 150,
        height: 600
      },
      content: data,
      pageMargins: [5, 5, 5, 5]
    };
    pdfMake.createPdf(docDefinition).print();

  }

  updateSusbscriptionState() {
    this.subscriptionService.updateSusbscriptionState(
      sessionStorage.getItem('companyId'),
      sessionStorage.getItem('parkingId'),
      this.subscriptionSelected
    ).subscribe(data => {
      this.getSubscriptions();
      this.toastService.show('Se canceló la suscripción exitosamente.', { classname: 'bg-success text-light' });
    }, error => {
      this.toastService.show('Error al cancelar la suscripción.', { classname: 'bg-danger text-light' });
    })
  }

  saveSubscription() {
    this.subscriptionService.saveSubscription(
      sessionStorage.getItem('companyId'),
      sessionStorage.getItem('parkingId'),
      { moneyReceived: this.moneyReceived, ...this.newSubscription }
    ).subscribe(data => {
      this.newSubscription = <Subscription>{};
      this.getSubscriptions();
      this.subscriptionInvoice = data;
      this.toastService.show('Se creo la suscripción.', { classname: 'bg-success text-light' });
      this.printSubscription(data);
      this.searchEnded = false;
    }, error => {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    })
  }

}
