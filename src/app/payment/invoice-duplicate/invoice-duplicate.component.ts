import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../../service/payment.service';
import { Operation } from '../../domain/operation';
import { CurrencyPipe } from '@angular/common';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as Utils from '../../../assets/utils/utils';

declare const $: any;
@Component({
  selector: 'app-invoice-duplicate',
  templateUrl: './invoice-duplicate.component.html',
  styleUrls: ['./invoice-duplicate.component.scss']
})
export class InvoiceDuplicateComponent implements OnInit {

  public searchBy: string;
  public operation: Operation;
  public operationPayments: [];
  public showInformationNotFound: boolean;
  public showLoading: boolean;

  constructor(public paymentService: PaymentService, private currencyPipe: CurrencyPipe) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.searchBy = 'Seleccione su opción';
    this.operation = new Operation('', sessionStorage.getItem('parkingId'), '', '', '');
    this.showInformationNotFound = false;
  }

  public onChange() {
    switch (this.searchBy) {
      case 'vehiclePlate':
        $(document).ready(() => {
          $("#vehiclePlateInput").focus();
        });
        break;
      case 'additionalCode':
        $(document).ready(() => {
          $("#additionalCodeInput").focus();
        });
        break;
      case 'operationCode':
        $(document).ready(() => {
          $("#codeInput").focus();
        });
        break;
    }
  }

  public searchPaymentsOperation() {
    this.showInformationNotFound = false;
    this.showLoading = true;
    let operationToSent = {
      'parkingId': this.operation.parkingId,
    }
    operationToSent[this.searchBy] = this.operation[this.searchBy]
    this.paymentService.findPaymentsOperation(operationToSent).subscribe(data => {
      this.operationPayments = data.operationPayments;
      this.showLoading = false;
    }, error => {
      this.showInformationNotFound = true;
      this.showLoading = false;
    })
  }

  public printPaymentTicket(operation) {
    const data = [];
    data.push([
      {
        text: operation.companyName,
        alignment: "center"
      },
      {
        text: `NIT: ${operation.companyTaxIdentification}`,
        alignment: "center"
      },
      {
        text: `No: ${operation.taxResolutionRangePrefix}-${
          operation.invoiceNumber
          }`,
        alignment: "center"
      },
      {
        text: operation.companyAddress,
        alignment: "center"
      },
      {
        text: operation.parkingEmail,
        alignment: "center"
      },
      {
        text: `Teléfono: ${operation.companyPhone}`,
        alignment: "center",
        margin: [0, 0, 0, 10]
      },
      {
        text: `Fecha: ${Utils.dateWithoutGMT(operation.created)}`,
        alignment: "left"
      },
      {
        text: `Ingreso: ${Utils.dateWithoutGMT(operation.entrance)}`,
        alignment: "left"
      },
      {
        text: `Matrícula: ${operation.vehiclePlate}`,
        alignment: "left"
      },
      {
        text: `Duración: ${Utils.secondsToHHmmss(operation.parkingTimeInSeconds)}`,
        alignment: "left"
      },
      {
        text: `Operario: ${operation.userName}`,
        alignment: "left"
      },
      {
        text: `Subtotal: ${this.currencyPipe.transform(operation.amount, 'COP', '$')}`,
        alignment: "left"
      },
      {
        text: `IVA: ${this.currencyPipe.transform(operation.tax, 'COP', '$')}`,
        alignment: "left"
      },
      {
        text: `TOTAL: ${this.currencyPipe.transform(operation.roundedTotal, 'COP', '$')}`,
        alignment: "left"
      },
      {
        text: `Recibido: ${this.currencyPipe.transform(operation.moneyReceived, 'COP', '$')}`,
        alignment: "left"
      },
      {
        text: `Valor a devolver: ${this.currencyPipe.transform(
          operation.moneyReturned, 'COP', '$'
        )}`,
        alignment: "left",
        margin: [0, 0, 0, 10]
      },
      {
        text: operation.serviceName,
        alignment: "center"
      },
      {
        text: operation.serviceFee,
        alignment: "center"
      },
      {
        text: `Número de Resolución: ${operation.taxResolutionNumber} de ${
          operation.taxResolutionDate
          }`,
        alignment: "center"
      },
      {
        text: `Prefijo ${operation.taxResolutionRangePrefix} Rango desde ${
          operation.taxResolutionRangeFrom
          } hasta ${operation.taxResolutionRangeTo}`,
        alignment: "center",
        margin: [0, 0, 0, 10]
      },
      {
        text: operation.parkingPolicy,
        alignment: "center",
        margin: [0, 0, 0, 10]
      },
      {
        text: "¡Gracias por su visita!",
        alignment: "center"
      }
    ]);
    const docDefinition = {
      pageOrientation: '',
      pageSize: {
        width: 150,
        height: 600
      },
      content: data,
      pageMargins: [5, 5, 5, 5]
    };
    pdfMake.createPdf(docDefinition).print();
  }

  public setFormatedDate(date) {
    return date.replace(" GMT-05:00", "");
  }

}
