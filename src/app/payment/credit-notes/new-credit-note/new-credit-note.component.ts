import { Component, OnInit } from '@angular/core';
import { CreditNote } from 'app/domain/creditNote';
import { PaymentService } from 'app/service/payment.service';
import { ToastService } from 'app/service/toast-service/toast-service';

@Component({
  selector: 'app-new-credit-note',
  templateUrl: './new-credit-note.component.html',
  styleUrls: ['./new-credit-note.component.scss']
})
export class NewCreditNoteComponent implements OnInit {

  public creditNote: CreditNote;

  constructor(
    private paymentService: PaymentService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.creditNote = <CreditNote>{}
    this.creditNote.total = 0;
  }

  onSave() {
    const parkingId = sessionStorage.getItem('parkingId');
    const type = 'CREDIT';
    this.paymentService.saveCreditNote({ parkingId, type, ...this.creditNote }).subscribe(data => {
      this.creditNote = <CreditNote>{}
      this.toastService.show('Se creo la nota de credito.', { classname: 'bg-success text-light' });
      this.paymentService.newCreditNote.emit(null);
    }, error => {
      if (error.error) {
        this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
      }
    })
  }

}
