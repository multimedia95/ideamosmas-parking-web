import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { PaymentService } from 'app/service/payment.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { CreditNote } from 'app/domain/creditNote';
import alasql from 'alasql';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

export interface CreditNotes {
  notes: CreditNote[],
  totalCollected: number
}

@Component({
  selector: 'app-credit-notes',
  templateUrl: './credit-notes.component.html',
  styleUrls: ['./credit-notes.component.scss']
})
export class CreditNotesComponent implements OnInit {

  public searchDate: Date;
  public showProcessingIcon: boolean;
  public parkingId: string = sessionStorage.getItem('parkingId');
  public creditNotes: CreditNotes;
  public creditNotesToDisplay: CreditNotes;
  public totalCollected: number;
  public lastSort: SortEvent;
  public filterText: string;
  currentPage = 1;
  itemsPerPage = 10;

  constructor(
    private paymentService: PaymentService,
    private toastService: ToastService
  ) { }

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  ngOnInit() {
    this.creditNotesToDisplay = { notes: [], totalCollected: 0 }
    this.creditNotes = { notes: [], totalCollected: 0 }
    this.showProcessingIcon = false;
    this.searchDate = new Date;
    this.findCreditNotes();
    this.lastSort = { column: '', direction: '' }
    this.filterText = '';
    this.paymentService.newCreditNote.subscribe(data => {
      this.findCreditNotes();
    })
  }

  onDateSelect(date) {
    this.searchDate.setFullYear(date.year, (date.month - 1), date.day);
    this.findCreditNotes();
  }

  onFilter(text) {
    this.creditNotesToDisplay.notes = this.creditNotes.notes.filter(creditNote => {
      return (
        (creditNote.created ? creditNote.created.toString().toLowerCase().includes(text) : false)
        || (creditNote.customerName ? creditNote.customerName.toLowerCase().includes(text) : false)
        || (creditNote.customerIdentification ? creditNote.customerIdentification.toString().toLowerCase().includes(text) : false)
        || (creditNote.customerPhone ? creditNote.customerPhone.toString().toLowerCase().includes(text) : false)
        || (creditNote.invoiceNumber ? creditNote.invoiceNumber.toString().toLowerCase().includes(text) : false)
        || (creditNote.description ? creditNote.description.toString().toLowerCase().includes(text) : false)
        || (creditNote.total ? creditNote.total.toString().toLowerCase().includes(text) : false)
        || (creditNote.username ? creditNote.username.toLowerCase().includes(text) : false)
      )
    })
  }

  onSort({ column, direction }: SortEvent) {

    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.lastSort = { column: column, direction: direction };

    if (direction === '') {
      this.creditNotesToDisplay.notes = this.creditNotes.notes.slice();
    } else {
      this.creditNotesToDisplay.notes = [...this.creditNotesToDisplay.notes].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  exportData() {
    const mystyle = {
      sheetid: "Notas de Crédito",
      headers: true,
      caption: {
        title: "Notas de Crédito"
      },
      style: "background:#ffffff",
      column: {
        style: "font-size:20px"
      },
      columns: [
        {
          columnid: "created",
          title: "Fecha"
        },
        {
          columnid: "customerName",
          title: "Nombre"
        },
        {
          columnid: "customerIdentification",
          title: "Cédula/NIT"
        },
        {
          columnid: "customerPhone",
          title: "Télefono"
        },
        {
          columnid: "rangePrefix",
          title: "Prefijo"
        },
        {
          columnid: "invoiceNumber",
          title: "No. Factura"
        },
        {
          columnid: "description",
          title: "Descripción"
        },
        {
          columnid: "total",
          title: "Valor"
        },
        {
          columnid: "username",
          title: "Usuario"
        }
      ],
      row: {
        style(sheet, row, rowidx) {
          return `background:${rowidx % 2 ? "white" : "white"}`;
        }
      },
      rows: {
        4: {
          cell: {
            style: "background:white"
          }
        }
      }
    };
    alasql('SELECT * INTO XLS("Nota de Crédito.xls",?) FROM ?', [
      mystyle,
      this.creditNotesToDisplay.notes
    ]);
  }

  findCreditNotes() {
    this.showProcessingIcon = true;
    const startDate = new Date(this.searchDate);
    const endDate = new Date(this.searchDate);
    startDate.setHours(0, 0, 0, 0);
    endDate.setHours(23, 59, 59, 999);
    this.paymentService.findCreditNotes(
      this.parkingId,
      startDate.toJSON(),
      endDate.toJSON()
    ).subscribe(data => {
      this.showProcessingIcon = false;
      let total = 0;
      data.notes.forEach(element => {
        total = total + element.total;
      });
      this.creditNotes.totalCollected = total;
      this.creditNotes.notes = data.notes;
      this.creditNotesToDisplay.totalCollected = total;
      this.creditNotesToDisplay.notes = data.notes;
    }, error => {
      this.showProcessingIcon = false;
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    })
  }
}
