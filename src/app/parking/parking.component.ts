import { Component, OnInit } from '@angular/core';
import { ParkingService } from 'app/service/parking.service';
import { ToastService } from 'app/service/toast-service/toast-service'
import { Operation } from '../domain/operation';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as Utils from '../../assets/utils/utils';
import { BlackListService } from 'app/service/black-list.service';

declare const $: any;
@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.scss']
})
export class ParkingComponent implements OnInit {
  public vehicleImages = [{
    path: '../../assets/img/grayCar2.png',
    caption: 'Carro',
    value: 'CAR',
    class: 'active'
  },
  {
    path: '../../assets/img/motorcycle2.png',
    caption: 'Moto',
    value: 'MOTORCYCLE',
    class: ''
  },
  {
    path: '../../assets/img/bicycle2.png',
    caption: 'Bicicleta',
    value: 'BICYCLE',
    class: ''
  },
  {
    path: '../../assets/img/taxi2.png',
    caption: 'Servicio Público',
    value: 'PUBLIC',
    class: ''
  },
  {
    path: '../../assets/img/truck2.png',
    caption: 'Camión',
    value: 'TRUCK',
    class: ''
  }];

  public exitImages = [{
    path: '../../assets/img/qrcode.png',
    caption: 'Código QR',
    class: 'active'
  },
  {
    path: '../../assets/img/vehicleplate.png',
    caption: 'Matricula',
    class: ''
  }];

  public exitType: string;
  public selectedVehicle: string;
  public entranceOperation: Operation;
  public exitOperation: Operation;
  public operationCode: string;
  public index: number;

  constructor(
    private parkingService: ParkingService,
    private toastService: ToastService,
    private blackListService: BlackListService
  ) { }

  ngOnInit() {
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.entranceOperation = new Operation('', sessionStorage.getItem('parkingId'), '', '', '');
    this.exitOperation = new Operation('', sessionStorage.getItem('parkingId'), '', '', '');
    this.operationCode = '';
    this.exitType = 'Código QR';
    if (this.vehicleImages.length > 0) {
      this.selectedVehicle = this.vehicleImages[0].value;
      this.index = 0;
    }
  }

  setVehicle(index) {
    this.selectedVehicle = this.vehicleImages[parseInt(index)].value;
    this.index = parseInt(index);
  }

  setExitType(index) {
    this.exitType = this.exitImages[parseInt(index)].caption;
  }

  printEntranceTicket(operation) {
    const data = [];
    data.push([
      {
        text: operation.companyName,
        alignment: "center"
      },
      {
        text: `NIT: ${operation.companyTaxIdentification}`,
        alignment: "center"
      },
      {
        text: `Dirección: ${operation.parkingAddress}`,
        alignment: "center"
      },
      {
        text: operation.parkingEmail,
        alignment: "center"
      },
      {
        text: `Teléfono: ${operation.parkingPhone}`,
        alignment: "center",
        margin: [0, 0, 0, 10]
      },
      {
        text: `Fecha: ${Utils.dateWithoutGMT(operation.created)}`,
        alignment: "left"
      },
      {
        text: `Matrícula: ${operation.vehiclePlate}`,
        alignment: "left"
      },
      {
        text: `Tipo de Vehiculo: ${Utils.setVehicleType(operation.vehicleType)}`,
        alignment: "left"
      },
      {
        text: `Operario: ${operation.userName}`,
        alignment: "left"
      },
      {
        qr: operation.operationCode,
        alignment: "center",
        margin: [0, 10, 0, 10]
      },
      {
        text: operation.subscriptionInformation === null ? '' : operation.subscriptionInformation,
        alignment: "center",
        margin: [0, 10, 0, 0]
      },
      {
        text: operation.parkingAdditionalInformation === null ? '' : operation.parkingAdditionalInformation,
        alignment: "center"
      }
    ]);
    const docDefinition = {
      pageOrientation: '',
      pageSize: {
        width: 150,
        height: 600
      },
      content: data,
      pageMargins: [5, 5, 5, 5]
    };
    pdfMake.createPdf(docDefinition).print();
  }

  onChangeSlide(action) {
    let indexToChange = 0;
    if (action === 'next') {
      indexToChange = this.index + 1 > this.vehicleImages.length - 1 ? 0 : this.index + 1;
      this.index = indexToChange;
      this.selectedVehicle = this.vehicleImages[indexToChange].value;
    } else if (action === 'prev') {
      indexToChange = this.index - 1 < 0 ? this.vehicleImages.length - 1 : this.index - 1;
      this.selectedVehicle = this.vehicleImages[indexToChange].value;
      this.index = indexToChange;
    }

  }

  saveEntranceOperation() {
    this.entranceOperation.vehiclePlate = this.entranceOperation.vehiclePlate.toUpperCase();
    this.entranceOperation.vehicleType = this.selectedVehicle;
    delete this.entranceOperation.operationCode;
    delete this.entranceOperation.additionalCode;
    this.parkingService.saveEntranceOperation(this.entranceOperation).subscribe(data => {
      this.blackListService.getVehicleReportByParkingIDAndPlate(this.entranceOperation.parkingId, this.entranceOperation.vehiclePlate).subscribe(
        data => {

        }
      )
      this.entranceOperation.vehiclePlate = '';
      this.printEntranceTicket(data)
      this.toastService.show('Se registró el vehículo.', { classname: 'bg-success text-light' });
    }, error => {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    });
  }

  saveExitOperation() {
    if (this.exitOperation.vehiclePlate) {
      this.exitOperation.vehiclePlate = this.exitOperation.vehiclePlate.toUpperCase();
    }
    delete this.exitOperation.vehicleType;
    delete this.exitOperation.operationCode;
    delete this.exitOperation.additionalCode;
    this.parkingService.saveExitOperation(this.exitOperation).subscribe(data => {
      this.toastService.show('Se registró la salida del vehículo.', { classname: 'bg-success text-light' });
    }, error => {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    })
  }
}
