import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UserInformationComponent } from './sidebar/user-information/user-information.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    FooterComponent,
    SidebarComponent,
    NavbarComponent,
    UserInformationComponent
  ],
  exports: [
    FooterComponent,
    SidebarComponent,
    NavbarComponent,
    UserInformationComponent
  ]
})
export class ComponentsModule { }
