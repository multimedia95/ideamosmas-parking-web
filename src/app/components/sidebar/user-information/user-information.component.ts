import { Component, OnInit, Input } from '@angular/core';
import { Parking } from 'app/domain/parking';

declare interface UserInfo {
  role: string;
  name: string;
  email: string;
  state: string;
  companyId: string;
  parkingList: Parking[]
}
@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent implements OnInit {
  @Input() userInformation: UserInfo;
  @Input() selectedParking: string;
  constructor() { }

  ngOnInit() {
  }

  onSelectParking(parking: Parking) {
    this.selectedParking = parking.name;
    sessionStorage.setItem('parkingId', parking.id);
    sessionStorage.setItem('configuration', parking.configuration);
  }
}
