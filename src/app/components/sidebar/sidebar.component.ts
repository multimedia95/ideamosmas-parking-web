import { Component, OnInit } from '@angular/core';
import { Parking } from 'app/domain/parking';
import { UserService } from 'app/service/user.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  subItems: any;
}

declare interface UserInfo {
  role: string;
  name: string;
  email: string;
  state: string;
  companyId: string;
  parkingList: Parking[]
}
export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard', title: 'Parking', icon: 'directions_car', class: '', subItems: [{ path: '/parking', title: 'Registro' },
    { path: '/dashboard', title: 'Pago' }, { path: '/credit-notes', title: 'Notas de Credito' }, { path: '/ticket', title: 'Duplicado de Tiquete' }, { path: '/invoice-duplicate', title: 'Duplicado de Factura' }]
  },
  {
    path: '/company', title: 'Informes', icon: 'view_headline', class: '', subItems: [{ path: '/agreements', title: 'Convenios' },
    { path: '/statistics', title: 'Estadisticas' }, { path: '/financial', title: 'Financieros' }, { path: '/operational', title: 'Operativos' }, { path: '/subscriptions', title: 'Suscripciones' },
    { path: '/sales', title: 'Resumen de Ventas' }, { path: '/daily-voucher', title: 'Comprobante Informe Diario' }]
  },
  {
    path: '/company', title: 'Suscripciones', icon: 'people_alt', class: '', subItems: [{ path: '/subscription', title: 'Crear' },
    { path: '/subscription-fare', title: 'Tarifas' }]
  },
  {
    path: '/company', title: 'Configuraciones', icon: 'settings', class: '', subItems: [{ path: '/agreements-settings', title: 'Convenios' },
    { path: '/resolution-settings', title: 'Resoluciones' }, { path: '/fare-settings', title: 'Tarifas' }, { path: '/fare-settings-special-date', title: 'Tarifas Especiales' },
    { path: '/users-settings', title: 'Usuarios' }, { path: '/roles', title: 'Roles' }, { path: '/attention-hours', title: 'Horarios' }]
  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public userInformation: UserInfo;
  selectedParking: string = '';
  constructor(public userService: UserService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.userService.findUserInformation().subscribe(data => {
      this.userInformation = { ...data }
      if (this.userInformation.parkingList.length > 0
        && !!!sessionStorage.getItem('parkingId')) {
        this.selectedParking = this.userInformation.parkingList[0].name;
        sessionStorage.setItem('parkingId', this.userInformation.parkingList[0].id);
        sessionStorage.setItem('configuration', this.userInformation.parkingList[0].configuration);
        sessionStorage.setItem('companyId', this.userInformation.companyId);
        sessionStorage.setItem('userName', this.userInformation.name);
      }

      if (this.selectedParking === '') {
        this.selectedParking = this.userInformation.parkingList.find(p => p.id === sessionStorage.getItem('parkingId')).name;
      }

      this.userService.finishLoading.emit(null);
    }, error => {
      console.log(error);
    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
