import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(public router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (sessionStorage.getItem('Authorization')) {
      return true
    } else {
      this.router.navigate(['']);
      return false;
    }
  }

  logOut() {
    this.router.navigate(['']);
    sessionStorage.removeItem('parkingId');
    sessionStorage.removeItem('companyId');
    sessionStorage.removeItem('userName');
    sessionStorage.removeItem('Authorization');
    sessionStorage.removeItem('Role');
    sessionStorage.removeItem('Expiration');
  }

}
