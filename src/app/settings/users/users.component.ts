import { Component, OnInit, Directive, Input, Output, EventEmitter } from '@angular/core';
import { NewUser } from 'app/domain/newUser';
import { Role } from 'app/domain/role';
import { ToastService } from 'app/service/toast-service/toast-service';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  showLoading: boolean = false;
  users: NewUser[] = [];
  newUser: NewUser;
  roles: Role[] = [];
  lastSort: SortEvent;
  page = 0;
  pageSize = 10;

  constructor(
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.newUser = {
      id: '',
      username: '',
      companyId: '',
      email: '',
      role: '',
      state: 'ENABLED'
    }

    this.lastSort = {
      column: '',
      direction: ''
    }
  }

  onSort(event) {

  }

  onSaveUser() {

  }

}
