import { Component, OnInit } from '@angular/core';
import { Schedule } from 'app/domain/schedule';
import { Time } from 'app/domain/time';

enum scheduleType {
  normal,
  specialDate,
  event
}

declare interface ScheduleConfiguration {
  idGroup: string;
  parkingId: string;
  schedules: Schedule[];
  name: string;
  editName: boolean;
  type: scheduleType;
  startDate: Date;
  endDate: Date;
  weekdayFrom: number;
  weekdayTo: number;
  hourRangeFrom: string;
  hourFrom: Time;
  hourRangeTo: string;
  hourTo: Time;
}


@Component({
  selector: 'app-attention-hours',
  templateUrl: './attention-hours.component.html',
  styleUrls: ['./attention-hours.component.scss']
})
export class AttentionHoursComponent implements OnInit {
  scheduleConfigurations: ScheduleConfiguration[] = [];
  newScheduleConfiguration: ScheduleConfiguration;
  basedOnSchedule: string = '';
  newScheduleName: string = '';
  scheduleTypes = [
    { name: 'Normal', value: 'normal' },
    { name: 'Evento', value: 'event' },
    { name: 'Fecha Especial', value: 'specialDate' },
  ]
  days = [
    { name: 'Domingo', value: 1 },
    { name: 'Lunes', value: 2 },
    { name: 'Martes', value: 3 },
    { name: 'Miercoles', value: 4 },
    { name: 'Jueves', value: 5 },
    { name: 'Viernes', value: 6 },
    { name: 'Sabado', value: 7 }
  ]
  constructor() { }

  ngOnInit() {
    this.scheduleConfigurations.push(
      {
        idGroup: null,
        parkingId: null,
        name: 'Horario Semana',
        editName: false,
        schedules: [],
        type: scheduleType.normal,
        startDate: null,
        endDate: null,
        weekdayFrom: null,
        weekdayTo: null,
        hourRangeFrom: '',
        hourRangeTo: '',
        hourFrom: { hour: 0, minute: 0 },
        hourTo: { hour: 0, minute: 0 }
      }
    )
    this.clearNewScheduled();
  }

  onAddSchedule(scheduleConfiguration: ScheduleConfiguration) {
    const schedule: Schedule = {
      startDate: null,
      endDate: null,
      hourRangeFrom: scheduleConfiguration.hourRangeFrom,
      hourRangeTo: scheduleConfiguration.hourRangeTo,
      hourFrom: scheduleConfiguration.hourFrom,
      hourTo: scheduleConfiguration.hourTo,
      weekdayFrom: scheduleConfiguration.weekdayFrom,
      weekdayTo: scheduleConfiguration.weekdayTo,
      id: null,
      parkingId: null,
      state: 'ENABLED'
    }

    scheduleConfiguration.schedules.push(schedule);
  }

  onRemoveSchedule(scheduleConfiguration: ScheduleConfiguration) {

  }

  onDateSelect(date, type, scheduleConfiguration: ScheduleConfiguration) {
    if (type === 'initial') {
      scheduleConfiguration.startDate = new Date();
      scheduleConfiguration.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      scheduleConfiguration.endDate = new Date();
      scheduleConfiguration.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  onCreateSchedule() {
    if (this.basedOnSchedule) {
      const scheduledConf = this.scheduleConfigurations.find(s => s.idGroup === this.basedOnSchedule)
      this.newScheduleConfiguration = scheduledConf
    }
    this.newScheduleConfiguration.name = this.newScheduleName;
    this.scheduleConfigurations.push(this.newScheduleConfiguration);
    this.clearNewScheduled();
  }

  clearNewScheduled() {
    this.basedOnSchedule = '';
    this.newScheduleName = '';
    this.newScheduleConfiguration = {
      idGroup: null,
      parkingId: null,
      name: '',
      editName: false,
      schedules: [],
      type: scheduleType.normal,
      startDate: null,
      endDate: null,
      weekdayFrom: null,
      weekdayTo: null,
      hourRangeFrom: '',
      hourRangeTo: '',
      hourFrom: { hour: 0, minute: 0 },
      hourTo: { hour: 0, minute: 0 }
    };
  }

  onChangeState(schedule: Schedule) {
    if (schedule.state === 'ENABLED') {
      schedule.state = 'DISABLED';
    } else if (schedule.state === 'DISABLED') {
      schedule.state = 'ENABLED';
    }
  }

  onEditName(scheduleConfiguration: ScheduleConfiguration) {
    scheduleConfiguration.editName = !scheduleConfiguration.editName;
  }

}
