import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementsSettingsComponent } from './agreements-settings.component';

describe('AgreementsSettingsComponent', () => {
  let component: AgreementsSettingsComponent;
  let fixture: ComponentFixture<AgreementsSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementsSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
