import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { SettingsService } from 'app/service/settings.service';
import { ToastService } from 'app/service/toast-service/toast-service';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

declare interface AgreementListItem {
  id: string;
  name: string;
  description: string;
  state: string;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-agreements-settings',
  templateUrl: './agreements-settings.component.html',
  styleUrls: ['./agreements-settings.component.scss']
})
export class AgreementsSettingsComponent implements OnInit {

  agreementList: AgreementListItem[] = [];
  agreementListToDisplay: AgreementListItem[] = [];
  agreement: AgreementListItem = {
    id: '',
    name: '',
    description: '',
    state: 'ENABLED'
  };
  showProcessingIcon: boolean = false;
  showInformationNotFound: boolean = false;
  onEdit: boolean = false;
  lastSort: SortEvent = { direction: '', column: '' };
  filterText: string = '';
  page = 1;
  pageSize = 10;
  states = [
    { id: 'ENABLED', value: 'Habilitado' },
    { id: 'DISABLED', value: 'Deshabilitado' }
  ]
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    private settingsService: SettingsService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.showProcessingIcon = true;
    this.getAgreements();
  }

  onFilter(textToFilter: string) {
    const text = textToFilter.toLocaleLowerCase();
    this.agreementListToDisplay = this.agreementList.filter(agreement => {
      return (
        (agreement.name ? agreement.name.toString().toLowerCase().includes(text) : false)
        || (agreement.description ? agreement.description.toString().toLowerCase().includes(text) : false)
      )
    })

    if (this.lastSort) {
      this.onSort(this.lastSort);
    }
  }

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.lastSort = { column: column, direction: direction };

    if (direction === '') {

    } else {
      this.agreementListToDisplay = [...this.agreementListToDisplay].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  onSelect(agreement: AgreementListItem) {
    this.agreement = agreement;
    this.onEdit = true;
  }

  onCancel() {
    this.agreement = {
      id: '',
      name: '',
      description: '',
      state: ''
    };
    this.onEdit = false;
  }

  getAgreements() {
    this.settingsService.findAgreements(sessionStorage.getItem("companyId")).subscribe(data => {
      this.agreementList = data.agreements;
      this.agreementListToDisplay = data.agreements;
      this.showProcessingIcon = false;
    }, error => {
      this.toastService.show("No se encontraron convenios configurados.", { classname: 'bg-warning text-light' });
      this.showProcessingIcon = false;
    })
  }

  onEditAgreement() {
    const agreement = {
      name: this.agreement.name,
      description: this.agreement.description,
      state: this.agreement.state
    }
    this.settingsService.updateAgreement(sessionStorage.getItem("companyId"), this.agreement.id, agreement).subscribe(
      () => {
        this.getAgreements();
        this.toastService.show("Se editó convenio.", { classname: 'bg-success text-light' });
        this.onCancel();
      }, error => {
        this.toastService.show("Falló al editar el convenio.", { classname: 'bg-danger text-light' });
      }
    )
  }

  onSave() {
    const agreement = {
      name: this.agreement.name,
      description: this.agreement.description
    }
    this.settingsService.saveAgreement(sessionStorage.getItem("companyId"), agreement).subscribe(
      () => {
        this.getAgreements();
        this.toastService.show("Se creo el convenio.", { classname: 'bg-success text-light' });
        this.onCancel();
      }, error => {
        this.toastService.show("Error al crear convenio.", { classname: 'bg-danger text-light' });
      }
    )
  }

}
