import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { SettingsService } from 'app/service/settings.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { TaxResolution } from 'app/domain/taxResolution';
import { DatePipe } from '@angular/common';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-resolution',
  templateUrl: './resolution.component.html',
  styleUrls: ['./resolution.component.scss']
})
export class ResolutionComponent implements OnInit {

  taxResolutions: TaxResolution[] = [];
  taxResolutionsToDisplay: TaxResolution[] = [];
  taxResolution: TaxResolution = {
    id: '',
    number: '',
    date: this.datePipe.transform(new Date(), 'y-MM-dd'),
    rangePrefix: '',
    rangeFrom: 0,
    rangeTo: 0,
    currentInvoiceNumber: 0,
    state: 'ENABLED'
  };
  showProcessingIcon: boolean = false;
  showInformationNotFound: boolean = false;
  onEdit: boolean = false;
  lastSort: SortEvent = { direction: 'asc', column: 'date' };
  filterText: string = '';
  page = 1;
  pageSize = 10;
  states = [
    { id: 'ENABLED', value: 'Habilitado' },
    { id: 'DISABLED', value: 'Deshabilitado' }
  ]
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    private settingsService: SettingsService,
    private toastService: ToastService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.showProcessingIcon = true;
    this.getResolutions();
  }

  onFilter(textToFilter: string) {
    const text = textToFilter.toLocaleLowerCase();
    this.taxResolutionsToDisplay = this.taxResolutions.filter(taxResolution => {
      return (
        (taxResolution.date ? taxResolution.date.toString().toLowerCase().includes(text) : false)
        || (taxResolution.currentInvoiceNumber ? taxResolution.currentInvoiceNumber.toString().toLowerCase().includes(text) : false)
      )
    })

    if (this.lastSort) {
      this.onSort(this.lastSort);
    }
  }

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.lastSort = { column: column, direction: direction };

    if (direction === '') {

    } else {
      this.taxResolutionsToDisplay = [...this.taxResolutionsToDisplay].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  onDateSelect(date) {
    this.taxResolution.date = `${date.year}-${(date.month - 1)}-${date.day}`;
  }

  onSelect(agreement: TaxResolution) {
    this.taxResolution = agreement;
    this.onEdit = true;
  }

  onCancel() {
    this.taxResolution = {
      id: '',
      number: '',
      date: this.datePipe.transform(new Date(), 'y-MM-dd'),
      rangePrefix: '',
      rangeFrom: 0,
      rangeTo: 0,
      currentInvoiceNumber: 0,
      state: 'ENABLED'
    };
    this.onEdit = false;
  }

  getResolutions() {
    this.settingsService.findResolutions(sessionStorage.getItem("companyId")).subscribe((data: { taxResolutions: [] }) => {
      this.taxResolutions = data.taxResolutions;
      this.taxResolutionsToDisplay = data.taxResolutions;
      this.showProcessingIcon = false;
      this.onSort(this.lastSort)
    }, error => {
      this.toastService.show("No se encontraron convenios configurados.", { classname: 'bg-warning text-light' });
      this.showProcessingIcon = false;
    })
  }

  onEditTaxResolution() {
    const resolution: TaxResolution = {
      ...this.taxResolution
    }
    resolution.date = new Date(this.taxResolution.date).toJSON();
    this.settingsService.updateResolution(sessionStorage.getItem("companyId"), resolution).subscribe(
      () => {
        this.toastService.show("Se editó resolución.", { classname: 'bg-success text-light' });
        this.getResolutions();
        this.onCancel();
      }, () => {
        this.toastService.show("Falló al editar de la resolución.", { classname: 'bg-danger text-light' });
      }
    )
  }

  onSave() {
    this.taxResolution.date = new Date(this.taxResolution.date).toJSON();
    this.settingsService.saveResolution(sessionStorage.getItem("companyId"), this.taxResolution).subscribe(
      () => {
        this.toastService.show("Se creo la resolución.", { classname: 'bg-success text-light' });
        this.getResolutions();
        this.onCancel();
      }, () => {
        this.toastService.show("Error al crear resolución.", { classname: 'bg-danger text-light' });
      }
    )
  }


}
