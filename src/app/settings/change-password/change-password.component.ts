import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';

declare interface UserPassword {
  currentPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
}

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public userPassword: UserPassword;
  constructor(public userService: UserService) { }

  ngOnInit() {
    this.userPassword = {
      currentPassword: '',
      newPassword: '',
      newPasswordConfirmation: ''
    }
  }

  validatePassword() {
    if (this.userPassword.currentPassword !== '' && (this.userPassword.newPassword === this.userPassword.newPasswordConfirmation)) {
      return true;
    } else {
      return false;
    }
  }

  updatePassword() {
    this.userService.updateUserPassword(
      sessionStorage.getItem('companyId'),
      this.userPassword
    ).subscribe(value => {
      console.log('Contraseña actualizada');
    }, error => {
      console.log('No se pudo actualizar la contraseña');
    })
  }

}
