import { Component, OnInit, Input, EventEmitter, Output, Directive, ViewChildren, QueryList } from '@angular/core';
import { Fare } from 'app/domain/fare';
import { SettingsService } from 'app/service/settings.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { PaymentService } from 'app/service/payment.service';
import { ActivatedRoute, Router } from '@angular/router';

declare interface FareConfiguration {
  constantTimeFares: Fare[];
  constantTimeFareSort: string;
  constantTimeFareOrder: number;
  constantTimeFarePage: number;
  constantTimeFare: Fare;
  expiredTimeFares: Fare[];
  expiredTimeFareSort: string;
  expiredTimeFareOrder: number;
  expiredTimeFarePage: number;
  expiredTimeFare: Fare;
  agreementsFares: Fare[];
  agreementsFareSort: string;
  agreementsFareOrder: number;
  agreementsFarePage: number;
  agreementFare: Fare;
  schedulesId: string[];
  scheduleId: string;
  paymentTypeId: string;
  name: string;
  editName: boolean;
  id: string;
}

declare interface AgreementListItem {
  id: string;
  name: string;
  description: string;
  state: string;
}

declare interface PaymentType {
  id: string;
  name: string;
  state: string;
  summary: string;
}

export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;


@Component({
  selector: 'app-flat-rate',
  templateUrl: './flat-rate.component.html',
  styleUrls: ['./flat-rate.component.scss']
})
export class FlatRateComponent implements OnInit {

  fareConfigurations: FareConfiguration[] = [];
  newFareConfiguration: FareConfiguration;
  newFareName: string = '';
  baseOnFare: string = '';
  fareType: string = '';
  agreementList: AgreementListItem[] = [];
  paymentTypes: PaymentType[];
  vehicleTypes: { name: string, value: string }[];
  configurations: { name: string, value: string, path: string }[] = [
    { name: 'Predeterminado', value: 'Default', path: 'fare-settings' },
    { name: 'Tarifa Única', value: 'flatRate', path: 'flat-rate' }
  ];
  configuration: string = 'flatRate';
  pageSize = 5;

  constructor(
    private settingsService: SettingsService,
    private toastService: ToastService,
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.fareConfigurations.push(
      {
        constantTimeFares: [],
        constantTimeFareOrder: 0,
        constantTimeFareSort: '',
        constantTimeFarePage: 1,
        constantTimeFare: {
          agreementId: '',
          fareGroupId: '',
          frame: 0,
          id: '',
          isOvertime: false,
          minuteFrom: 0,
          minuteTo: 0,
          paymentTypeId: '',
          roundingType: 'CEIL',
          roundingValue: 50,
          scheduleId: '',
          state: 'ENABLED',
          value: 0,
          vehicleTypeId: ''
        },
        expiredTimeFares: [],
        expiredTimeFareOrder: 0,
        expiredTimeFareSort: '',
        expiredTimeFarePage: 1,
        expiredTimeFare: {
          agreementId: '',
          fareGroupId: '',
          frame: 0,
          id: '',
          isOvertime: false,
          minuteFrom: 0,
          minuteTo: 0,
          paymentTypeId: '',
          roundingType: 'CEIL',
          roundingValue: 50,
          scheduleId: '',
          state: 'ENABLED',
          value: 0,
          vehicleTypeId: ''
        },
        agreementsFares: [],
        agreementsFareOrder: 0,
        agreementsFareSort: '',
        agreementsFarePage: 1,
        agreementFare: {
          agreementId: '',
          fareGroupId: '',
          frame: 0,
          id: '',
          isOvertime: false,
          minuteFrom: 0,
          minuteTo: 0,
          paymentTypeId: '',
          roundingType: 'CEIL',
          roundingValue: 50,
          scheduleId: '',
          state: 'ENABLED',
          value: 0,
          vehicleTypeId: ''
        },
        schedulesId: [],
        scheduleId: '',
        name: 'Configuración Semana',
        editName: false,
        paymentTypeId: '',
        id: null
      }
    )
    this.fareType = this.route.snapshot.data['fareType'];
    this.clearNewFare();
    this.getAgreements();
    this.getPaymentTypes();
    const configuration = JSON.parse(sessionStorage.getItem('configuration'));
    if (configuration && configuration.vehicleTypes) {
      this.vehicleTypes = configuration.vehicleTypes
    }
  }

  getAgreements() {
    this.settingsService.findAgreements(sessionStorage.getItem('companyId')).subscribe(data => {
      this.agreementList = data.agreements.filter(a => a.state === 'ENABLED');
    }, error => {
      this.toastService.show('No se encontraron convenios.', { classname: 'bg-warning text-light' });
    })
  }

  getPaymentTypes() {
    this.paymentService.findPaymentTypes().subscribe(data => {
      this.paymentTypes = data.filter(a => a.state === 'ENABLED');;
    }, error => {

    })
  }

  onEditName(fareConfiguration: FareConfiguration) {
    fareConfiguration.editName = !fareConfiguration.editName;
  }

  onCreateFare() {
    if (this.baseOnFare) {
      const fareConf = this.fareConfigurations.find(f => f.id === this.baseOnFare)
      this.newFareConfiguration = fareConf
    }
    this.newFareConfiguration.name = this.newFareName;
    this.fareConfigurations.push(this.newFareConfiguration);
    this.clearNewFare();
  }

  clearNewFare() {
    this.baseOnFare = '';
    this.newFareName = '';
    this.newFareConfiguration = {
      constantTimeFares: [],
      constantTimeFareSort: '',
      constantTimeFareOrder: 0,
      constantTimeFarePage: 1,
      constantTimeFare: {
        agreementId: '',
        fareGroupId: '',
        frame: 0,
        id: '',
        isOvertime: false,
        minuteFrom: 0,
        minuteTo: 0,
        paymentTypeId: '',
        roundingType: 'CEIL',
        roundingValue: 50,
        scheduleId: '',
        state: 'ENABLED',
        value: 0,
        vehicleTypeId: ''
      },
      expiredTimeFares: [],
      expiredTimeFareSort: '',
      expiredTimeFareOrder: 0,
      expiredTimeFarePage: 1,
      expiredTimeFare: {
        agreementId: '',
        fareGroupId: '',
        frame: 0,
        id: '',
        isOvertime: true,
        minuteFrom: 0,
        minuteTo: 0,
        paymentTypeId: '',
        roundingType: 'CEIL',
        roundingValue: 50,
        scheduleId: '',
        state: 'ENABLED',
        value: 0,
        vehicleTypeId: ''
      },
      agreementsFares: [],
      agreementsFareSort: '',
      agreementsFareOrder: 0,
      agreementsFarePage: 1,
      agreementFare: {
        agreementId: '',
        fareGroupId: '',
        frame: 0,
        id: '',
        isOvertime: false,
        minuteFrom: 0,
        minuteTo: 0,
        paymentTypeId: '',
        roundingType: 'CEIL',
        roundingValue: 50,
        scheduleId: '',
        state: 'ENABLED',
        value: 0,
        vehicleTypeId: ''
      },
      schedulesId: [],
      scheduleId: '',
      paymentTypeId: '',
      name: '',
      editName: false,
      id: ''
    }
  }

  onChangeState(fare: Fare) {
    if (fare.state === 'ENABLED') {
      fare.state = 'DISABLED';
    } else if (fare.state === 'DISABLED') {
      fare.state = 'ENABLED';
    }
  }

  onAddConstantFare(fareConfiguration: FareConfiguration) {
    const constantTimeFare = {
      agreementId: '',
      fareGroupId: '',
      frame: 0,
      id: '',
      isOvertime: false,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: fareConfiguration.constantTimeFare.value,
      vehicleTypeId: fareConfiguration.constantTimeFare.vehicleTypeId
    }
    fareConfiguration.constantTimeFares.push(constantTimeFare);

    fareConfiguration.constantTimeFare = {
      agreementId: '',
      fareGroupId: '',
      frame: 0,
      id: '',
      isOvertime: false,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: 0,
      vehicleTypeId: ''
    }
  }

  onAddExpirationFare(fareConfiguration: FareConfiguration) {
    const expirationTimeFare = {
      agreementId: '',
      fareGroupId: '',
      frame: fareConfiguration.expiredTimeFare.frame,
      id: '',
      isOvertime: true,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: fareConfiguration.expiredTimeFare.value,
      vehicleTypeId: fareConfiguration.expiredTimeFare.vehicleTypeId
    }
    fareConfiguration.expiredTimeFares.push(expirationTimeFare);
    fareConfiguration.expiredTimeFare = {
      agreementId: '',
      fareGroupId: '',
      frame: 0,
      id: '',
      isOvertime: true,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: 0,
      vehicleTypeId: ''
    }
  }

  onAddAgreement(fareConfiguration: FareConfiguration) {
    const agreementFare = {
      agreementId: fareConfiguration.agreementFare.agreementId,
      fareGroupId: '',
      frame: 0,
      id: '',
      isOvertime: false,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: fareConfiguration.agreementFare.value,
      vehicleTypeId: fareConfiguration.agreementFare.vehicleTypeId
    }
    fareConfiguration.agreementsFares.push(agreementFare);
    fareConfiguration.agreementFare = {
      agreementId: '',
      fareGroupId: '',
      frame: 0,
      id: '',
      isOvertime: false,
      minuteFrom: 0,
      minuteTo: 0,
      paymentTypeId: '',
      roundingType: 'CEIL',
      roundingValue: 50,
      scheduleId: '',
      state: 'ENABLED',
      value: 0,
      vehicleTypeId: ''
    }
  }

  onAddSchedule(fareConfiguration: FareConfiguration) {
    const scheduleId = fareConfiguration.scheduleId;
    fareConfiguration.schedulesId.push(scheduleId);
    fareConfiguration.scheduleId = '';
  }

  onSelectConfig() {
    const config = this.configurations.find(c => c.value === this.configuration);
    this.router.navigate([config.path])
  }

  onSort(column, config: FareConfiguration, fareName) {
    if (column === config[fareName + 'Sort']) {
      switch (config[fareName + 'Order']) {
        case 1:
          config[fareName + 'Order'] = -1;
          break;
        case -1:
          config[fareName + 'Order'] = 1;
          break;
      }
    } else {
      config[fareName + 'Sort'] = column;
      config[fareName + 'Order'] = 1;
    }

    if (config[fareName + 'Order'] === 0) {
    } else {
      config[fareName + 's'] = [...config[fareName + 's']].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return config[fareName + 'Order'] === 1 ? res : -res;
      });
    }

  }

}
