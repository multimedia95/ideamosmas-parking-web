export class SalesSummary {
    constructor(
        public parkingName: string,
        public taxIdentification: string,
        public parkingAddress: string,
        public taxResolutionNumber: string,
        public taxResolutionDate: string,
        public taxResolutionRangePrefix: string,
        public taxResolutionRangeFrom: number,
        public taxResolutionRangeTo: number,
        public created: string,
        public userName: string,
        public startTime: string,
        public endTime: string,
        public invoiceRangeFrom: number,
        public invoiceRangeTo: number,
        public creditNoteRangeFrom: number,
        public creditNoteRangeTo: number,
        public cashBoxBalance: number,
        public totalSales: number,
        public totalTaxedSales: number,
        public totalDontTaxedSales: number,
        public creditNotes: number,
        public taxedCreditNotes: number,
        public dontTaxedCreditNotes: number,
        public netSales: number,
        public tax: number,
        public expenses: number,
        public total: number,
        public date: string
    ) { }
}