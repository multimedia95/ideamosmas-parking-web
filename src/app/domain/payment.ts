export class Payment {
    constructor(
        public id: string,
        public operationId: string,
        public vehicleType: string,
        public vehiclePlate: string,
        public created: Date,
        public serviceName: string,
        public additionalCode: string,
        public invoiceNumber: string,
        public amount: number,
        public tax: number,
        public roundedTotal: number,
        public userName: string,
        public parkingTimeInSeconds: number,
        public parkingTime: string
    ) { }
}