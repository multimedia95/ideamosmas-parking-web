export class Agreement {
    constructor(
        public operationId: string,
        public vehicleType: string,
        public vehiclePlate: string,
        public created: Date,
        public serviceName: string,
        public parkingTime: string,
        public parkingTimeInSeconds: number,
        public serviceCourtesyTime: string,
        public serviceCourtesyTimeInSeconds: number,
        public agreementTime: string,
        public agreementTimeInSeconds: number
    ) { }
}