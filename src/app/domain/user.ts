export class User {
    constructor(
        public username: string,
        public email: string,
        public name: string,
        public nameCompany: string
    ) { }
}