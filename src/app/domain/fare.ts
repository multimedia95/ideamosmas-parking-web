export class Fare {
    constructor(
        public id: string,
        public state: string,
        public minuteFrom: number,
        public minuteTo: number,
        public frame: number,
        public value: number,
        public roundingType: string,
        public roundingValue: number,
        public isOvertime: boolean,
        public vehicleTypeId: string,
        public agreementId: string,
        public paymentTypeId: string,
        public scheduleId: string,
        public fareGroupId: string
    ) { }
}