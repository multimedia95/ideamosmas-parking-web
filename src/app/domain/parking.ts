export class Parking {
    constructor(
        public id: string,
        public created: string,
        public address: string,
        public email: string,
        public name: string,
        public phone: string,
        public nameCompany: string,
        public configuration: string
    ) { }
}