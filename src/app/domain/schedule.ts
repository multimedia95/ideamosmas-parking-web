import { Time } from "./time";

export class Schedule {
    constructor(
        public id: string,
        public state: string,
        public startDate: Date,
        public endDate: Date,
        public weekdayFrom: number,
        public weekdayTo: number,
        public hourRangeFrom: string,
        public hourRangeTo: string,
        public hourFrom: Time,
        public hourTo: Time,
        public parkingId: string
    ) { }
}