export class SubscriptionReport {
    constructor(
        public created: string,
        public start: string,
        public end: string,
        public state: string,
        public parkingId: string,
        public vehiclePlate: string,
        public additionalCode: string,
        public subscriptionPriceName: string,
        public quantity: string,
        public amount: number,
        public total: number,
        public tax: number,
        public userName: string,
        public invoiceNumber: string,
    ) { }
}