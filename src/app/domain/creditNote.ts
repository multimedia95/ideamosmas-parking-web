export class CreditNote {
    constructor(
        public id: string,
        public created: string,
        public customerName: string,
        public customerIdentification: string,
        public customerPhone: string,
        public rangePrefix: string,
        public invoiceNumber: number,
        public description: string,
        public total: number,
        public username: string,
        public consecutive: number
    ) { }
}