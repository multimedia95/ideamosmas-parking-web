export class Role {
    constructor(
        public id: string,
        public summary: string,
        public companyId: string,
        public state: string
    ) { }
}