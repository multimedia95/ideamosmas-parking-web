export class OperationReport {
    constructor(
        public id: string,
        public additionalCode: string,
        public parkingId: string,
        public state: string,
        public userName: string,
        public vehiclePlate: string,
        public vehicleType: string,
        public entrance: string,
        public exit: string
    ) { }
}
