export class TaxResolution {
    constructor(
        public id: string,
        public number: string,
        public date: string,
        public rangePrefix: string,
        public rangeFrom: number,
        public rangeTo: number,
        public currentInvoiceNumber: number,
        public state: string
    ) { }
}