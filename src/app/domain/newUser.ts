export class NewUser {
    constructor(
        public id: string,
        public username: string,
        public email: string,
        public role: string,
        public companyId: string,
        public state: string
    ) { }
}