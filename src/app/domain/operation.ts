export class Operation {
    constructor(
        public operationCode: string,
        public parkingId: string,
        public vehicleType: string,
        public vehiclePlate: string,
        public additionalCode: string
    ) { }
}
