export class Subscription {
    constructor(
        public id: string,
        public vehiclePlate: string,
        public additionalCode: string,
        public subscriptionPriceId: string,
        public amount: number,
        public tax: number,
        public total: number,
        public subscriptionState: string,
        public start: Date,
        public end: Date,
        public quantity: number,
        public parkingId: string,
        public invoiceNumber: string,
        public username: string,
        public created: Date,
        public parkingTimeInSeconds: string
    ) { }
}