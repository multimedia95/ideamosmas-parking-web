import { Component, OnInit } from '@angular/core';
import { Login } from 'app/domain/login';
import { LoginServiceService } from 'app/service/login-service.service';
import { ToastService } from 'app/service/toast-service/toast-service'
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public login: Login;
  public passwordRecoveryInfo: Login;
  constructor(
    public loginService: LoginServiceService,
    public router: Router,
    public toastService: ToastService
  ) { }

  ngOnInit() {
    this.login = new Login('', '');
    this.passwordRecoveryInfo = new Login('', '');
    sessionStorage.clear();
  }

  public doLogin() {
    if (this.login.username === '') {
      this.toastService.show('Llene todos los campos por favor.', { classname: 'bg-warning text-light' });
    } else {
      this.loginService.getLogin(this.login).subscribe(data => {

        sessionStorage.setItem('Authorization', data.Authorization);
        sessionStorage.setItem('Role', data.role);
        sessionStorage.setItem('Expiration', data.expiration);
        this.router.navigate(['dashboard'])

      }, error => {
        this.toastService.show('Datos Erroneos', { classname: 'bg-danger text-light' });
        this.login.username = ('');
        this.login.password = ('');
      })
    }
  }

  public passwordRecovery() {
    if (this.passwordRecoveryInfo.username === '') {
      this.toastService.show('Ingrese nombre de usuario', { classname: 'bg-warning text-light' });
    } else {
      this.loginService.passwordRecovery(this.passwordRecoveryInfo);
      this.passwordRecoveryInfo.username = ('');
      $('#passwordRecoveryModal').modal('hide')
    }
  }
}
