import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class PaymentService {
    public newCreditNote: EventEmitter<any> = new EventEmitter();
    public urlNode: string;
    public url: string;
    public urlInit: string;
    public urlPayment: string;
    public httpOptions = {
        headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('Authorization')
        })
    }

    constructor(public httpClient: HttpClient) {
        this.urlNode = environment.nodeUrl + 'report/';
        this.url = environment.apiUrl;
        this.urlInit = environment.urlInit;
        this.urlPayment = environment.paymentApi;
    }

    public findPaymentsOperation(data): Observable<any> {
        const url = `${this.urlInit}operation/payment`;
        return this.httpClient.post(url, data, {
            headers: {
                Authorization: sessionStorage.getItem('Authorization')
            }
        });
    }

    // Credit Notes
    public findCreditNotes(parkingId, from, to): Observable<any> {
        const url = `${this.urlInit}payment/note/parking/${parkingId}/credit?&from=${from}&to=${to}`;
        return this.httpClient.get(url, this.httpOptions);
    }

    public saveCreditNote(creditNote): Observable<any> {
        const url = `${this.urlInit}payment/note`;
        return this.httpClient.post(url, creditNote, this.httpOptions);
    }

    // https://prueba-payment.azurewebsites.net/

    public findPaymentTypes(): Observable<any> {
        const url = `${this.urlPayment}paymentType/findAll`;
        return this.httpClient.get(url, this.httpOptions);
    }

}