import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SubscriptionService {

    public urlNode: string;
    public url: string;
    public urlInit: string;
    public httpOptions = {
        headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('Authorization')
        })
    }

    constructor(public httpClient: HttpClient) {
        this.urlNode = environment.nodeUrl + 'report/';
        this.url = environment.apiUrl;
        this.urlInit = environment.urlInit;
    }

    public findSubscriptions(parkingId): Observable<any> {
        const url = `${this.urlInit}company/${sessionStorage.getItem(
            'companyId'
        )}/parking/${parkingId}/subscription/`;
        return this.httpClient.get(url, this.httpOptions);
    }

    public findSubscriptionPrices(companyId): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/subscription/price/`;
        return this.httpClient.get(url, this.httpOptions);
    }

    public findSubscriptionById(companyId, parkingId, subscriptionId): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/parking/${parkingId}/subscription/${subscriptionId}/payment`;
        return this.httpClient.get(url, this.httpOptions);
    }

    public updateSusbscriptionState(companyId, parkingId, subscription): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/parking/${parkingId}/subscription/${subscription.id}/disable`;
        return this.httpClient.post(url, subscription, this.httpOptions);
    }

    public saveSubscription(companyId, parkingId, subscription): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/parking/${parkingId}/subscription/`;
        return this.httpClient.post(url, subscription, this.httpOptions)
    }

    public subscriptionInformation(companyId, parkingId, subscription): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/parking/${parkingId}/subscription/calculate/`;
        return this.httpClient.post(url, subscription, this.httpOptions);
    }

    public createSubscriptionPrice(companyId, subscriptionPrice): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/subscription/price/`;
        return this.httpClient.post(url, subscriptionPrice, this.httpOptions)
    }

    public updateSubscriptionPrice(companyId, id, subscriptionPrice): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/subscription/price/${id}`;
        return this.httpClient.post(url, subscriptionPrice, this.httpOptions)
    }

}