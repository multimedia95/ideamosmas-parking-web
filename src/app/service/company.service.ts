import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  public urlNode: string;
  public url: string;


  constructor(public httpClient: HttpClient) {
    this.urlNode = environment.nodeUrl + 'report/';
    this.url = environment.apiUrl;
  }

  public countCompanies(): Observable<any> {
    return this.httpClient.get(this.urlNode + 'company/countCompany')
  }

  public fiveLastCompanies(): Observable<any> {
    return this.httpClient.get(this.urlNode + 'company/findfivelastCompanies');
  }

  public findAllCompanies(): Observable<any> {
    return this.httpClient.get(this.urlNode + 'company/findAllCompanies');
  }

  public companiesByMonth(): Observable<any> {
    return this.httpClient.get(this.urlNode + 'company/quantityLastCompanies');
  }

}
