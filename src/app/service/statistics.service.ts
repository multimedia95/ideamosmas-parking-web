import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService {

    public urlNode: string;
    public url: string;
    public urlInit: string;
    public reportUrl: string;
    public httpOptions = {
        headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('Authorization')
        })
    }

    constructor(public httpClient: HttpClient) {
        this.urlNode = environment.nodeUrl + 'report/';
        this.url = environment.apiUrl;
        this.urlInit = environment.urlInit;
        this.reportUrl = environment.reportUrl;
    }

    public findVehiclesParkedByType(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclesParkedByType?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findVehiclesQuantityByDate(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclesQuantityByDate?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findTotalPaymentsByDate(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/totalPaymentsByDate?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findTotalSubscriptionsByDate(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/totalSubscriptionsByDate?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findQuantityByVehicleTypes(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/quantityByVehicleTypes?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findPaymentsByPaymentTypes(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/paymentsByPaymentTypes?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findVehiclesPaymentsGroupedByDate(parkingId, from, to): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclesPaymentsGroupedByDate?from=${from}&to=${to}`;
        return this.httpClient.get(url)
    }

    public findVehiclesPaymentsGroupedByRange(parkingId, from, days): Observable<any> {
        const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclesPaymentsGroupedByRange?from=${from}&days=${days}`;
        return this.httpClient.get(url)
    }
}
