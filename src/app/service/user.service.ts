import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public urlNode: string;
  public url: string;
  public urlInit: string;
  public finishLoading: EventEmitter<any> = new EventEmitter();
  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': sessionStorage.getItem('Authorization')
    })
  }

  constructor(public httpClient: HttpClient) {
    this.urlNode = environment.nodeUrl + 'report/';
    this.url = environment.apiUrl;
    this.urlInit = environment.urlInit;
  }

  public findUserInformation(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': sessionStorage.getItem('Authorization')
      })
    }
    return this.httpClient.get(this.urlInit + 'user', this.httpOptions)
  }
  public updateUserPassword(companyId, userPassword): Observable<any> {
    return this.httpClient.post(`${this.urlInit}company/${companyId}/user/password`, userPassword, this.httpOptions)
  }

  public findUsersEnabled(companyId): Observable<any> {
    const url = `${this.urlInit}company/${companyId}/user/enabled`;
    return this.httpClient.get(url, this.httpOptions)
  }
}
