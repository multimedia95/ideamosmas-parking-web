import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlackListService {

  public url: string;
  public apiPasword: string;
  public headers: Headers = new Headers();

  constructor(private httpClient: HttpClient) {
    this.url = environment.apiBlackListUrl;
    this.apiPasword = environment.apiPasword;
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
  }

  public getDocumentTypes(): Observable<any> {
    const url = `${this.url}select/document_types?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getGender(): Observable<any> {
    const url = `${this.url}select/genders?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getRegions(): Observable<any> {
    const url = `${this.url}select/regions?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getRiskTypes(): Observable<any> {
    const url = `${this.url}select/risk_types?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getVehiclesTypes(): Observable<any> {
    const url = `${this.url}select/vehicles_types?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getReasonsReport(): Observable<any> {
    const url = `${this.url}reasons_report/select?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getVehicleReportsByParkingID(parkingId): Observable<any> {
    const url = `${this.url}vehicle/report/select?api=${this.apiPasword}&parking_id_prod=${parkingId}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getAllVehicleReports(): Observable<any> {
    const url = `${this.url}vehicle/report/select?api=${this.apiPasword}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public getVehicleReportByParkingIDAndPlate(parkingId, vehiclePlate): Observable<any> {
    const url = `${this.url}vehicle/report/select?api=${this.apiPasword}&license_plate=${vehiclePlate}&parking_id_prod=${parkingId}`;
    return this.httpClient.post(url, { headers: this.headers });
  }

  public saveVehicleReport(vehicleReport): Observable<any> {
    const url = `${this.url}vehicle/report/insert?api=${this.apiPasword}&private_information=${vehicleReport.privateInformation}&reason_report_id=${vehicleReport.reasonReportId}&observations=${vehicleReport.observations}&creater=${vehicleReport.creater}&parking_id_prod=${vehicleReport.parkingId}&license_plate=${vehicleReport.licensePlate}&vehicle_type_id=${vehicleReport.vehicleTypeId}`;

    return this.httpClient.post(url, { headers: this.headers });
  }

  public updateVehicleReport(vehicleReport): Observable<any> {
    const url = `${this.url}vehicle/report/update?api=${this.apiPasword}&private_information=${vehicleReport.privateInformation}&region_id=${vehicleReport.regionId}&reason_report_id=${vehicleReport.reasonReportId}&observations=${vehicleReport.observations}&modifier=${vehicleReport.modifier}&state=${vehicleReport.state}&id=${vehicleReport.id}`;

    return this.httpClient.post(url, { headers: this.headers })
  }

}
