import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    public urlNode: string;
    public url: string;
    public urlInit: string;
    public reportUrl: string;
    public httpOptions = {
        headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('Authorization')
        })
    }

    constructor(public httpClient: HttpClient) {
        this.urlNode = environment.nodeUrl + 'report/';
        this.url = environment.apiUrl;
        this.urlInit = environment.urlInit;
        this.reportUrl = environment.reportUrl;
    }

    // Agreements 
    public findAgreements(companyId): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/agreement`;
        return this.httpClient.get(url, this.httpOptions)
    }

    public updateAgreement(companyId, agreementId, agreement): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/agreement/${agreementId}`;
        return this.httpClient.post(url, agreement, this.httpOptions);
    }

    public saveAgreement(companyId, agreement): Observable<any> {
        const url = `${this.urlInit}company/${companyId}/agreement/`;
        return this.httpClient.post(url, agreement, this.httpOptions);
    }

    // Resolution
    public findResolutions(companyId) {
        const url = `${this.urlInit}company/${companyId}/tax-resolution`;
        return this.httpClient.get(url, this.httpOptions);
    }

    public findEnabledResolutions(companyId) {
        const url = `${this.urlInit}company/${companyId}/tax-resolution/ENABLED`;
        return this.httpClient.get(url, this.httpOptions);
    };
    public updateResolution(companyId, resolution) {
        const url = `${this.urlInit}company/${companyId}/tax-resolution/${resolution.id}`;
        return this.httpClient.post(url, resolution, this.httpOptions);
    };

    public saveResolution(companyId, resolution) {
        const url = `${this.urlInit}company/${companyId}/tax-resolution/`;
        return this.httpClient.post(url, resolution, this.httpOptions);
    };

}
