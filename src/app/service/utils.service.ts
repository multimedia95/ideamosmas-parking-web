import { PdfMake } from 'pdfmake';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    constructor(public pdfMake: PdfMake) {

    }

    setState(state): any {
        let response;
        switch (state) {
            case "ENABLED":
                response = "Habilitado";
                break;
            case "DISABLED":
                response = "Deshabilitado";
                break;
        }
        return response;
    }

    setRole(state): any {
        let response;
        switch (state) {
            case "ADMIN":
                response = "Administrador";
                break;
            case "VENDOR":
                response = "Vendedor";
                break;
            case "SUPERVISOR":
                response = "Supervisor";
                break;
            case "MASTER":
                response = "Master";
                break;
        }
        return response;
    }

    setDate(date: any) {
        let dateFormatted = "";
        if (date != null) {
            dateFormatted = new Date(date).toLocaleString();
        }
        return dateFormatted;
    }

    toLocaleString(date): any {
        const d = new Date(date);

        const options = {
            year: "numeric",
            month: "2-digit",
            day: "numeric",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit"
        };

        return d.toLocaleString("zu", options);
    }

    formatDate(date): any {
        let d = new Date(date),
            month = `${d.getMonth() + 1}`,
            day = `${d.getDate()}`,
            year = d.getFullYear();

        if (month.length < 2) month = `0${month}`;
        if (day.length < 2) day = `0${day}`;

        return [year, month, day].join("-");
    }

    public dateWithoutGMT(date): any {
        return date.replace(" GMT-05:00", "");
    }

    secondsToHHmmss(totalSeconds): any {
        const hours = Math.floor(totalSeconds / 3600);
        const minutes = Math.floor((totalSeconds - hours * 3600) / 60);
        let seconds = totalSeconds - hours * 3600 - minutes * 60;

        seconds = Math.round(seconds * 100) / 100;
        const response = `${hours}h ${minutes}m ${seconds}s`;
        return response;
    }

    minutesToSeconds(minutes): any {
        const seconds = Math.floor(minutes * 60);
        return seconds;
    }

    setVehicleType(vehicleType): any {
        switch (vehicleType) {
            case "CAR":
                return "Carro";
            case "MOTORCYCLE":
                return "Moto";
            case "BICYCLE":
                return "Bicicleta";
            case "TRUCK":
                return "Camión";
            case "PUBLIC":
                return "Servicio Público";
            default:
                return "Otro";
        }
    }

    setPaymentType(vehicleType): any {
        switch (vehicleType) {
            case "NORMAL":
                return "Normal";
            case "AGREEMENT":
                return "Convenio";
            case "SUBSCRIPTION":
                return "Suscripción";
            default:
                return "Otro";
        }
    }

    setTime(time): any {
        switch (time) {
            case "DAY":
                return "Día";
            case "MONTH":
                return "Mes";
            case "TRIMESTER":
                return "Trimestre";
            case "SEMESTER":
                return "Semestre";
            case "YEAR":
                return "Año";
            default:
                return "Otro";
        }
    }

    textNullToNull(text): any {
        if (text === "null") {
            return null;
        }
        return text;
    }

    doPdf(title, additionalInformation, landscape): any {
        const docDefinition = {
            pageOrientation: landscape,
            footer: {
                text: "www.ideamosmas.com",
                alignment: "center"
            },
            content: [
                {
                    text: title,
                    style: "header"
                },
                {
                    text: "Numbered list example:"
                },
                {
                    text: additionalInformation,
                    style: "header"
                }
            ],
            styles: {
                header: {
                    fontSize: 22,
                    bold: true,
                    alignment: "center",
                    margin: [5, 2, 10, 20]
                }
            }
        };

        this.pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
        this.pdfMake.createPdf(docDefinition).open();
    }

    ticketToPdf(title, data, landscape): any {
        const docDefinition = {
            pageOrientation: landscape,
            pageSize: {
                width: 150,
                height: 600
            },
            content: data,
            pageMargins: [5, 5, 5, 5]
        };
        this.pdfMake.createPdf(docDefinition).print();
    }

    convertTableToPdf(title, data, additionalInformation, landscape): any {
        const docDefinition = {
            pageOrientation: landscape,
            footer: {
                text: "www.ideamosmas.com",
                alignment: "center"
            },
            content: [
                {
                    text: title,
                    style: "header"
                },
                {
                    table: {
                        body: data
                    }
                },
                {
                    text: additionalInformation,
                    style: "header"
                }
            ],
            styles: {
                header: {
                    fontSize: 22,
                    bold: true,
                    alignment: "center",
                    margin: [5, 2, 10, 20]
                }
            }
        };

        this.pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
        this.pdfMake.createPdf(docDefinition).open();
    }

    convertTableToXls(title, columns, data): any {
        const mystyle = {
            sheetid: title,
            headers: true,
            caption: {
                title
            },
            style: "background:#ffffff",
            column: {
                style: "font-size:20px"
            },
            columns,
            row: {
                style(sheet, row, rowidx) {
                    return `background:${rowidx % 2 ? "white" : "white"}`;
                }
            },
            rows: {
                4: {
                    cell: {
                        style: "background:white"
                    }
                }
            }
        };
        // alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, data]);
    }
}