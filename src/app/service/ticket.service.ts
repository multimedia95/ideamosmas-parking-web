import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class TicketService {
    public urlNode: string;
    public url: string;
    public urlInit: string;
    public httpOptions = {
        headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('Authorization')
        })
    }

    constructor(public httpClient: HttpClient) {
        this.urlNode = environment.nodeUrl + 'report/';
        this.url = environment.apiUrl;
        this.urlInit = environment.urlInit;
    }

    public findEntranceOperation(data): Observable<any> {
        const url = `${this.urlInit}operation/entrance/find`;
        return this.httpClient.post(url, data, this.httpOptions);
    }

}