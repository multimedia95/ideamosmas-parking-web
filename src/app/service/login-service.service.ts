import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'environments/environment';
import { Login } from 'app/domain/login';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  public url: string;
  public login: Login;

  constructor(public httpClient: HttpClient) {
    // this.url = environment.apiUrl;
    this.url = environment.urlInit;
  }

  public getLogin(login: Login): Observable<any> {
    return this.httpClient.post(this.url + 'login', login);
  }

  public passwordRecovery(user) {
    return this.httpClient.post(this.url + 'user/password/recovery', { username : user.username})
  }

}
