import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  public urlNode: string;
  public url: string;
  public urlInit: string;
  public reportUrl: string;
  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': sessionStorage.getItem('Authorization')
    })
  }

  constructor(public httpClient: HttpClient) {
    this.urlNode = environment.nodeUrl + 'report/';
    this.url = environment.apiUrl;
    this.urlInit = environment.urlInit;
    this.reportUrl = environment.reportUrl;
  }

  // Agreements 
  public findAgreements(companyId): Observable<any> {
    const url = `${this.urlInit}company/${companyId}/agreement`;
    return this.httpClient.get(url, this.httpOptions)
  }

  public findAgreementsReport(parkingId, agreementId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/agreement/${agreementId}/payment`;
    return this.httpClient.post(url, data);
  }

  public findAgreementsReportToExport(parkingId, agreementId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/agreement/${agreementId}/payment/all`;
    return this.httpClient.post(url, data);
  }

  // Financial
  public findFinancialReport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/payment`;
    return this.httpClient.post(url, data);
  }

  public findFinancialReportToExport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/payment/all`;
    return this.httpClient.post(url, data);
  }

  // Subscriptions
  public findSubscriptions(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/subscriptions`;
    return this.httpClient.post(url, data);
  }

  public findSubscriptionsToExport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/subscriptions/all`;
    return this.httpClient.post(url, data);
  }

  // Sales
  public findSalesSummary(companyId, parkingId, data): Observable<any> {
    const url = `${this.urlInit}report/company/${companyId}/parking/${parkingId}/user/sales`;
    return this.httpClient.post(url, data, this.httpOptions);
  };

  // Daily Voucher
  public findDailyVoucher(companyId, parkingId, data): Observable<any> {
    const url = `${this.urlInit}report/company/${companyId}/parking/${parkingId}/sales`;
    return this.httpClient.post(url, data, this.httpOptions);
  }

  // Operational
  public findOperationsReport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation`;
    return this.httpClient.post(url, data);
  }

  public findOperationsReportToExport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/all`;
    return this.httpClient.post(url, data);
  }

  public findOperationsByEntranceNumber(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/entranceNumber`;
    return this.httpClient.post(url, data);
  }

  public findOperationsByEntranceNumberToExport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/entranceNumber/all`;
    return this.httpClient.post(url, data);
  }

  public findOperationsByParkingTime(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/parkingTime`;
    return this.httpClient.post(url, data);
  }

  public findOperationsByParkingTimeToExport(parkingId, data): Observable<any> {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/parkingTime/all`;
    return this.httpClient.post(url, data);
  }

  public updateExitOperation(data): Observable<any> {
    const url = `${this.urlInit}operation/exit/forced`;
    return this.httpClient.post(url, data, this.httpOptions);
  }

  public updateVehicleType(data): Observable<any> {
    const url = `${this.urlInit}operation/vehicle-plate/update`;
    return this.httpClient.post(url, data, this.httpOptions);
  }

  public findOperationByVehiclePlate(parkingId, data) {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclePlate`;
    return this.httpClient.post(url, data);
  };

  public findOperationByVehiclePlateToExport(parkingId, data) {
    const url = `${this.reportUrl}report/parking/${parkingId}/operation/vehiclePlate/all`;
    return this.httpClient.post(url, data);
  };

}
