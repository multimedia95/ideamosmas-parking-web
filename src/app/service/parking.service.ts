import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParkingService {

  public urlNode: string;
  public url: string;
  public urlInit: string;
  public httpOptions = {
    headers : new HttpHeaders({
      'Authorization' : sessionStorage.getItem('Authorization')
    })
  }

  constructor(public httpClient: HttpClient) {
    this.urlNode = environment.nodeUrl + 'report/';
    this.url = environment.apiUrl;
    this.urlInit = environment.urlInit;
  }

  public saveEntranceOperation(data): Observable<any> {
    const formData = new FormData();
    formData.append('operation', JSON.stringify(data));
    return this.httpClient.post(`${this.urlInit}operation/entrance`,  formData, this.httpOptions)
  }

  public saveExitOperation(data): Observable<any> {
    const formData = new FormData();
    formData.append('operation', JSON.stringify(data));
    return this.httpClient.post(`${this.urlInit}operation/exit`, formData, this.httpOptions)
  }
}
