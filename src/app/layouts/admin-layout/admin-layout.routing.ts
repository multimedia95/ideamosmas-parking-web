import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { AuthGuard } from 'app/guards/auth.guard';
import { ParkingComponent } from 'app/parking/parking.component';
import { ChangePasswordComponent } from '../../settings/change-password/change-password.component';
import { FaqsComponent } from '../../settings/faqs/faqs.component';
import { SubscriptionComponent } from '../../subscriptions/subscription/subscription.component';
import { InvoiceDuplicateComponent } from '../../payment/invoice-duplicate/invoice-duplicate.component';
import { TicketComponent } from '../../ticket/ticket.component'
import { AgreementsComponent } from '../../reports/agreements/agreements.component';
import { StatisticsComponent } from '../../reports/statistics/statistics.component';
import { FinancialComponent } from '../../reports/financial/financial.component';
import { OperationalComponent } from '../../reports/operational/operational.component';
import { SubscriptionsComponent } from '../../reports/subscriptions/subscriptions.component';
import { SalesSummaryComponent } from '../../reports/sales-summary/sales-summary.component';
import { DailyVoucherComponent } from '../../reports/daily-voucher/daily-voucher.component';
import { CreditNotesComponent } from 'app/payment/credit-notes/credit-notes.component';
import { SubscriptionFareComponent } from 'app/subscriptions/subscription-fare/subscription-fare.component';
import { AgreementsSettingsComponent } from 'app/settings/agreements-settings/agreements-settings.component';
import { ResolutionComponent } from 'app/settings/resolution/resolution.component';
import { FareComponent } from 'app/settings/fare/fare.component';
import { UsersComponent } from 'app/settings/users/users.component';
import { AttentionHoursComponent } from 'app/settings/attention-hours/attention-hours.component';
import { FlatRateComponent } from 'app/settings/flat-rate/flat-rate.component';
import { RolesComponent } from 'app/settings/roles/roles.component';


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'subscription', component: SubscriptionComponent, canActivate: [AuthGuard] },
    { path: 'parking', component: ParkingComponent, canActivate: [AuthGuard] },
    { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard] },
    { path: 'faqs', component: FaqsComponent, canActivate: [AuthGuard] },
    { path: 'invoice-duplicate', component: InvoiceDuplicateComponent, canActivate: [AuthGuard] },
    { path: 'ticket', component: TicketComponent, canActivate: [AuthGuard] },
    { path: 'agreements', component: AgreementsComponent, canActivate: [AuthGuard] },
    { path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuard] },
    { path: 'financial', component: FinancialComponent, canActivate: [AuthGuard] },
    { path: 'operational', component: OperationalComponent, canActivate: [AuthGuard] },
    { path: 'subscriptions', component: SubscriptionsComponent, canActivate: [AuthGuard] },
    { path: 'subscription-fare', component: SubscriptionFareComponent, canActivate: [AuthGuard] },
    { path: 'sales', component: SalesSummaryComponent, canActivate: [AuthGuard] },
    { path: 'daily-voucher', component: DailyVoucherComponent, canActivate: [AuthGuard] },
    { path: 'credit-notes', component: CreditNotesComponent, canActivate: [AuthGuard] },
    { path: 'agreements-settings', component: AgreementsSettingsComponent, canActivate: [AuthGuard] },
    { path: 'resolution-settings', component: ResolutionComponent, canActivate: [AuthGuard] },
    { path: 'flat-rate', component: FlatRateComponent, canActivate: [AuthGuard] },
    { path: 'fare-settings', component: FareComponent, canActivate: [AuthGuard], data: { fareType: 'normal' } },
    { path: 'fare-settings-special-date', component: FareComponent, canActivate: [AuthGuard], data: { fareType: 'specialDate' } },
    { path: 'users-settings', component: UsersComponent, canActivate: [AuthGuard] },
    { path: 'roles', component: RolesComponent, canActivate: [AuthGuard] },
    { path: 'attention-hours', component: AttentionHoursComponent, canActivate: [AuthGuard] }
];
