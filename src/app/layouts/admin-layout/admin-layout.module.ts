import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { ParkingComponent } from 'app/parking/parking.component';
import { ChangePasswordComponent } from '../../settings/change-password/change-password.component';
import { FaqsComponent } from '../../settings/faqs/faqs.component';
import { SubscriptionComponent } from '../../subscriptions/subscription/subscription.component';
import { InvoiceDuplicateComponent } from '../../payment/invoice-duplicate/invoice-duplicate.component';
import { TicketComponent } from '../../ticket/ticket.component';
import { AgreementsComponent } from '../../reports/agreements/agreements.component';
import { StatisticsComponent } from '../../reports/statistics/statistics.component';
import { FinancialComponent } from '../../reports/financial/financial.component';
import { OperationalComponent } from '../../reports/operational/operational.component';
import { SubscriptionsComponent } from '../../reports/subscriptions/subscriptions.component';
import { SalesSummaryComponent } from '../../reports/sales-summary/sales-summary.component';
import { DailyVoucherComponent } from '../../reports/daily-voucher/daily-voucher.component';

import { NgbdSortableHeader } from '../../subscriptions/subscription/subscription.component'

import { CreditNotesComponent } from 'app/payment/credit-notes/credit-notes.component';
import { NewCreditNoteComponent } from 'app/payment/credit-notes/new-credit-note/new-credit-note.component';
import { SubscriptionFareComponent } from 'app/subscriptions/subscription-fare/subscription-fare.component';
import { ResolutionComponent } from 'app/settings/resolution/resolution.component';
import { FareComponent } from 'app/settings/fare/fare.component';
import { UsersComponent } from 'app/settings/users/users.component';
import { AgreementsSettingsComponent } from 'app/settings/agreements-settings/agreements-settings.component';
import { AttentionHoursComponent } from 'app/settings/attention-hours/attention-hours.component';
import { FlatRateComponent } from 'app/settings/flat-rate/flat-rate.component';
import { RolesComponent } from 'app/settings/roles/roles.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  declarations: [
    DashboardComponent,
    ParkingComponent,
    ChangePasswordComponent,
    FaqsComponent,
    SubscriptionComponent,
    InvoiceDuplicateComponent,
    TicketComponent,
    AgreementsComponent,
    StatisticsComponent,
    FinancialComponent,
    OperationalComponent,
    SubscriptionsComponent,
    SalesSummaryComponent,
    DailyVoucherComponent,
    CreditNotesComponent,
    NewCreditNoteComponent,
    SubscriptionFareComponent,
    AgreementsSettingsComponent,
    ResolutionComponent,
    FareComponent,
    UsersComponent,
    NgbdSortableHeader,
    AttentionHoursComponent,
    FlatRateComponent,
    RolesComponent
  ]
})

export class AdminLayoutModule { }
