import { Component, OnInit } from '@angular/core';
import { ToastService } from 'app/service/toast-service/toast-service';
import { ReportService } from 'app/service/report.service';
import { UserService } from 'app/service/user.service';
import { SalesSummary } from 'app/domain/salesSummary';
import { DatePipe, CurrencyPipe } from '@angular/common';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
  user: string;
}

declare interface User {
  id: string,
  role: string,
  name: string,
  email: string,
  state: string,
  username: string
}

@Component({
  selector: 'app-sales-summary',
  templateUrl: './sales-summary.component.html',
  styleUrls: ['./sales-summary.component.scss']
})
export class SalesSummaryComponent implements OnInit {

  public searchParameters: SearchParameters;
  public users: User[];
  public companyId: string;
  public parkingId: string;
  public cashBalance: number;
  public expenses: number;
  public showProcessingIcon: boolean;
  public salesSummary: SalesSummary;


  constructor(
    private toastService: ToastService,
    private reportService: ReportService,
    private userService: UserService,
    private datePipe: DatePipe,
    private currencyPipe: CurrencyPipe
  ) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.companyId = sessionStorage.getItem('companyId');
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { startDate: new Date(), endDate: new Date(), user: '' };
    this.searchParameters.startDate.setHours(0, 0, 0, 0);
    this.searchParameters.endDate.setHours(23, 59, 59, 999);
    this.showProcessingIcon = false;
    this.cashBalance = 0;
    this.expenses = 0;
    this.findUsers();
  }

  verifyMaximumRangeDate() {

    const from = this.searchParameters.startDate;
    const to = this.searchParameters.endDate;
    const maxDate = new Date(from);
    maxDate.setHours(maxDate.getHours() + 24);

    if (maxDate >= to) {
      return true;
    }

    this.toastService.show('El limite de tiempo es de 24 horas.', { classname: 'bg-warning text-light' });
  }

  findUsers() {
    this.userService.findUsersEnabled(sessionStorage.getItem('companyId')).subscribe(
      data => {
        this.users = data.users;
      },
      () => {
        this.toastService.show('No se encontraron usuarios', { classname: 'bg-warning text-light' });
      }
    );
  }

  onDateSelect(date, type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      this.searchParameters.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  onSearch() {
    if (this.verifyMaximumRangeDate()) {
      this.showProcessingIcon = true;
      const data = {
        from: this.searchParameters.startDate,
        to: this.searchParameters.endDate,
        creator: this.searchParameters.user,
        cashBoxBalance: this.cashBalance,
        expenses: this.expenses
      };

      this.reportService.findSalesSummary(
        this.companyId,
        this.parkingId,
        data
      ).subscribe(data => {
        this.showProcessingIcon = false;
        this.salesSummary = data;
        this.salesSummary.date = `${this.datePipe.transform(this.searchParameters.startDate, 'y-MM-dd HH:mm')} - ${this.datePipe.transform(this.searchParameters.endDate, 'y-MM-dd HH:mm')}`
      }, error => {
        this.showProcessingIcon = false;
        if (error.error) {
          this.toastService.show(error.error.message, { classname: 'bg-warning text-light' });
        }
      })
    }
  }

  doPdf() {
    const cashBalance = this.cashBalance;
    const expenses = this.expenses;
    let totalInvoice = 0;

    if (this.salesSummary.invoiceRangeTo != 0 && this.salesSummary.invoiceRangeTo != null) {
      totalInvoice =
        this.salesSummary.invoiceRangeTo -
        this.salesSummary.invoiceRangeFrom +
        1;
    }

    let totalCreditNotes = 0;

    if (this.salesSummary.creditNoteRangeTo != 0) {
      totalCreditNotes =
        this.salesSummary.creditNoteRangeTo -
        this.salesSummary.creditNoteRangeFrom +
        1;
    }
    const docDefinition = {
      footer: {
        text: 'www.ideamosmas.com',
        alignment: 'center'
      },
      content: [
        {
          text: this.salesSummary.parkingName,
          style: 'center'
        },
        {
          text: `NIT: ${this.salesSummary.taxIdentification}`,
          style: 'center'
        },
        {
          text: this.salesSummary.parkingAddress,
          style: 'center'
        },
        {
          text: `Número de Resolución: ${
            this.salesSummary.taxResolutionNumber
            } de ${this.salesSummary.taxResolutionDate}`,
          style: 'center'
        },
        {
          text: `Prefijo ${
            null != this.salesSummary.taxResolutionRangePrefix ? this.salesSummary.taxResolutionRangePrefix : ''
            } Rango desde ${this.salesSummary.taxResolutionRangeFrom} hasta ${
            this.salesSummary.taxResolutionRangeTo
            }`,
          style: 'center'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: '*** CUADRE DE CAJA ***',
          style: 'center'
        },
        {
          text: `Fecha: ${this.salesSummary.date}`,
          style: 'left'
        },
        {
          text: `Responsable:  ${this.salesSummary.userName}`,
          style: 'left'
        },
        {
          text: `Hora Inicial:  ${this.salesSummary.startTime.slice(0, 8)}`,
          style: 'left'
        },
        {
          text: `Hora Final:  ${this.salesSummary.endTime.slice(0, 8)}`,
          style: 'left'
        },
        {
          text: `Rango de Facturas: ${
            null != this.salesSummary.invoiceRangeFrom ? this.salesSummary.invoiceRangeFrom : ''
            } - ${null != this.salesSummary.invoiceRangeTo ? this.salesSummary.invoiceRangeTo : ''}`,
          style: 'left'
        },
        {
          text: `Rango de Notas Crédito: ${
            this.salesSummary.creditNoteRangeFrom
            } - ${this.salesSummary.creditNoteRangeTo}`,
          style: 'left'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: 'TOTALES CONSOLIDADOS',
          style: 'center'
        },
        {
          text: `Saldo en Caja: ${this.currencyPipe.transform(
            this.salesSummary.cashBoxBalance, 'COP', '$'
          )}`,
          style: 'left'
        },
        {
          text: `(+) Ventas Totales: ${this.currencyPipe.transform(
            this.salesSummary.totalSales, 'COP', '$'
          )}`,
          style: 'left'
        },
        {
          text: `(-) Devoluciones: ${this.currencyPipe.transform(
            this.salesSummary.creditNotes, 'COP', '$'
          )}`,
          style: 'left'
        },
        {
          text: `(=) Ventas Netas: ${this.currencyPipe.transform(
            this.salesSummary.netSales, 'COP', '$'
          )}`,
          style: 'left'
        },
        {
          text: `(+) IVA: ${this.currencyPipe.transform(this.salesSummary.tax, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `(-) Pagos: ${this.currencyPipe.transform(
            this.salesSummary.expenses, 'COP', '$'
          )}`,
          style: 'left'
        },
        {
          text: `TOTAL EN CAJA: ${this.currencyPipe.transform(
            this.salesSummary.total, 'COP', '$'
          )}`,
          style: 'left'
        }
      ],
      styles: {
        header: {
          bold: true,
          alignment: 'center',
          margin: [5, 2, 2, 5]
        },
        center: {
          alignment: 'center',
          margin: [5, 2, 2, 5]
        },
        left: {
          alignment: 'left',
          margin: [5, 2, 2, 5]
        },
        right: {
          alignment: 'right',
          margin: [5, 2, 2, 5]
        }
      }
    };

    pdfMake.createPdf(docDefinition).open();
    pdfMake
      .createPdf(docDefinition)
      .download('Comprobante informe diario' + '.pdf');
  }

}
