import { Component, OnInit } from '@angular/core';
import { ReportService } from 'app/service/report.service';
import { OperationReport } from 'app/domain/operationReport';
import { ToastService } from 'app/service/toast-service/toast-service';
import alasql from 'alasql';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as Utils from '../../../assets/utils/utils';

export type SortDirection = 1 | -1;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

declare interface OperationByEntranceNumber {
  count: string;
  vehicleType: string;
  vehiclePlate: string;
}

declare interface OperationByParkingTime {
  vehiclePlate: string;
  vehicleType: string;
  parkingTimeInSeconds: number;
  parkingTime: string;
}

declare interface OperationByVehiclePlate {
  entrance: string;
  parkingTimeInSeconds: number;
  parkingTime: string;
}
declare interface Operations {
  operations: OperationReport[];
  totalItems: number;
}

declare interface OperationsByEntranceNumber {
  operations: OperationByEntranceNumber[];
  totalItems: number;
}

declare interface OperationsByParkingTime {
  operations: OperationByParkingTime[];
  totalItems: number;
}

declare interface OperationsByVehiclePlate {
  operations: OperationByVehiclePlate[];
  totalItems: number;
}


@Component({
  selector: 'app-operational',
  templateUrl: './operational.component.html',
  styleUrls: ['./operational.component.scss']
})
export class OperationalComponent implements OnInit {
  public parkingId: string;
  public operationsReport: Operations;
  public operationsByEntranceNumber: OperationsByEntranceNumber;
  public operationsByParkingTime: OperationsByParkingTime;
  public operationsByVehiclePlate: OperationsByVehiclePlate;
  public totalItems: number;
  public operationToEdit: OperationReport;
  public operationToClose: OperationReport;
  public icons = [
    {
      name: 'CAR',
      icon: 'directions_car'
    },
    {
      name: 'MOTORCYCLE',
      icon: 'motorcycle'
    },
    {
      name: 'BICYCLE',
      icon: 'directions_bike'
    },
    {
      name: 'PUBLIC',
      icon: 'local_taxi'
    },
    {
      name: 'TRUCK',
      icon: 'local_shipping'
    }
  ]
  public generalInformation = {
    currentPage: 0,
    itemsPerPage: 4,
    from: new Date(),
    to: new Date(),
    sort: "entrance",
    order: -1,
    filter: ''
  }
  public vehiclesByEntranceNumber = {
    currentPage: 0,
    itemsPerPage: 4,
    from: new Date(),
    to: new Date(),
    gte: 1,
    sort: "count",
    order: -1,
    filter: ''
  };
  public entryByParkingTime = {
    currentPage: 0,
    itemsPerPage: 4,
    from: new Date(),
    to: new Date(),
    gte: 1,
    sort: "parkingTime",
    order: -1,
    filter: ''
  }
  public entryByVehiclePlate = {
    currentPage: 0,
    itemsPerPage: 4,
    from: new Date(),
    to: new Date(),
    vehiclePlate: 'IDM123',
    sort: "entrance",
    order: -1,
    filter: ''
  }

  currentPage = 1;
  currentPageVehiclesByEntrance = 1;
  currentPageEntryByParkingTime = 1;
  currentPageEntryByVehiclePlate = 1;

  constructor(
    private reportService: ReportService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.parkingId = sessionStorage.getItem('parkingId');
    this.generalInformation.from.setHours(0, 0, 0, 0);
    this.generalInformation.to.setHours(23, 59, 59, 999);
    this.vehiclesByEntranceNumber.from.setHours(0, 0, 0, 0);
    this.vehiclesByEntranceNumber.to.setHours(23, 59, 59, 999);
    this.entryByParkingTime.from.setHours(0, 0, 0, 0);
    this.entryByParkingTime.to.setHours(23, 59, 59, 999);
    this.entryByVehiclePlate.from.setHours(0, 0, 0, 0);
    this.entryByVehiclePlate.to.setHours(23, 59, 59, 999);
    this.operationsReport = { operations: [], totalItems: 0 };
    this.operationsByEntranceNumber = { operations: [], totalItems: 0 };
    this.operationsByParkingTime = { operations: [], totalItems: 0 };
    this.operationsByVehiclePlate = { operations: [], totalItems: 0 };
    this.operationToEdit = {
      id: "",
      additionalCode: "",
      parkingId: "",
      state: "",
      userName: "",
      vehiclePlate: "",
      vehicleType: "",
      entrance: "",
      exit: ""
    }
    this.operationToClose = {
      id: "",
      additionalCode: "",
      parkingId: "",
      state: "",
      userName: "",
      vehiclePlate: "",
      vehicleType: "",
      entrance: "",
      exit: ""
    }
    this.findGeneralInformation();
    this.getOperationsByEntranceNumber();
    this.getOperationsByParkingTime();
    this.getOperationsByVehiclePlate();
  }

  onDateSelect(date, type, object) {
    if (type === 'initial') {
      object.from = new Date(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      object.to = new Date(date.year, (date.month - 1), date.day, 23, 59);
    }
  }

  onSort(column, object, functionToCall) {
    if (column === object.sort) {
      switch (object.order) {
        case 1:
          object.order = -1;
          break;
        case -1:
          object.order = 1;
          break;
      }
    } else {
      object.sort = column;
      object.order = 1;
    }

    switch (functionToCall) {
      case 'generalInformation':
        this.findGeneralInformation();
        break;
      case 'entranceNumber':
        this.getOperationsByEntranceNumber();
        break;
      case 'parkingTime':
        this.getOperationsByParkingTime();
        break;
      case 'vehiclePlate':
        this.getOperationsByVehiclePlate();
        break;
    }
  }

  onFilter(event, object, functionToCall) {
    if (object.filter.length >= 3) {
      if (event.key === 'Enter') {
        switch (functionToCall) {
          case 'generalInformation':
            this.findGeneralInformation();
            break;
          case 'entranceNumber':
            this.getOperationsByEntranceNumber();
            break;
          case 'parkingTime':
            this.getOperationsByParkingTime();
            break;
          case 'vehiclePlate':
            this.getOperationsByVehiclePlate();
            break;
        }
      }
    }
  }
  // General information
  exportGeneralInformationToPdf() {
    this.getGeneralInformationToExport(this.parkingId, operations => {
      const title = "Informe de Operaciones";
      const additionalInformation = "";
      const sourceData = operations;
      const data = [];
      data.push([
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tipo de Vehículo",
          bold: true
        },
        {
          text: "Hora de Entrada",
          bold: true
        },
        {
          text: "Hora de Salida",
          bold: true
        }
      ]);

      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(Utils.setVehicleType(sourceRow.vehicleType));
        dataRow.push(sourceRow.entrance);
        if (sourceRow.exit != null) {
          dataRow.push(sourceRow.exit);
        } else {
          dataRow.push("");
        }
        data.push(dataRow);
      });

      const docDefinition = {
        pageOrientation: "",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    });
  }

  exportGeneralInformationToXls() {
    this.getGeneralInformationToExport(this.parkingId, operations => {
      const title = "Informe de Operaciones";
      const columns = [
        {
          columnid: "id"
        },
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "vehicleType",
          title: "Tipo de Vehículo"
        },
        {
          columnid: "entrance",
          title: "Hora de Entrada"
        },
        {
          columnid: "exit",
          title: "Hora de Salida"
        }
      ];
      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, operations]);
    });
  }

  setIcon(vehicleType) {
    return this.icons.find(icon => icon.name === vehicleType).icon;
  }

  removeActiveClass(element: HTMLElement) {
    element.classList.remove('active');
  }

  findGeneralInformation() {
    this.generalInformation.currentPage = this.currentPage - 1;
    this.reportService.findOperationsReport(this.parkingId, this.generalInformation).subscribe(
      (data: Operations) => {
        this.operationsReport = data;
      }, error => {
        if (error.status !== 404) {
          this.toastService.show(error.message, { classname: 'bg-warning text-light' });
        }
      }
    );
  }

  getGeneralInformationToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const generalInformationToExport = {
      from: this.generalInformation.from,
      to: this.generalInformation.to,
      sort: "entrance",
      order: -1,
      filter: ''
    }
    if (this.generalInformation.filter.length >= 3) {
      generalInformationToExport.filter = this.generalInformation.filter;
    }

    this.reportService.findOperationsReportToExport(parkingId, generalInformationToExport).subscribe((data: OperationReport[]) => {
      data.forEach(element => {
        element.entrance = element.entrance
        if (element.exit !== null) {
          element.exit = element.exit;
        }
      })
      callback(data)
    }, error => {
      if (error) {
        this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
      }
    })

  }

  saveVehiclePlate() {
    const data = {
      operationId: this.operationToEdit.id,
      parkingId: this.parkingId,
      vehiclePlate: this.operationToEdit.vehiclePlate
    }

    this.reportService.updateVehicleType(data).subscribe(data => {
      this.operationToEdit = {
        id: "",
        additionalCode: "",
        parkingId: "",
        state: "",
        userName: "",
        vehiclePlate: "",
        vehicleType: "",
        entrance: "",
        exit: ""
      };
      this.findGeneralInformation();
      this.toastService.show("Se modificó la matrícula.", { classname: 'bg-success text-light' });
    }, error => {
      this.findGeneralInformation();
      this.toastService.show("No es posible modificar la matrícula.", { classname: 'bg-warning text-light' });
    })
  }

  updateExitOperation() {
    const exitOperation = {
      operationId: this.operationToClose.id,
      parkingId: this.parkingId
    }
    this.reportService.updateExitOperation(exitOperation).subscribe(
      () => {
        this.findGeneralInformation();
        this.toastService.show("Se registró la salida.", { classname: 'bg-success text-light' });
      }, () => {
        this.toastService.show("Falló al cerrar la operación.", { classname: 'bg-warning text-light' });
      }
    )
  }
  // Vehicles by entrance number
  getOperationsByEntranceNumber() {
    this.vehiclesByEntranceNumber.currentPage = this.currentPageVehiclesByEntrance - 1;
    this.reportService.findOperationsByEntranceNumber(this.parkingId, this.vehiclesByEntranceNumber).subscribe(data => {
      this.operationsByEntranceNumber = data;
    }, error => {
      if (error) {
        this.toastService.show(error.message, { classname: 'bg-warning text-light' });
      }
    })
  }

  getOperationsByEntranceNumberToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const vehiclesByEntranceNumberToExport = {
      from: this.vehiclesByEntranceNumber.from,
      to: this.vehiclesByEntranceNumber.to,
      gte: this.vehiclesByEntranceNumber.gte,
      sort: this.vehiclesByEntranceNumber.sort,
      order: this.vehiclesByEntranceNumber.order,
      filter: ''
    };
    if (this.vehiclesByEntranceNumber.filter.length >= 3) {
      vehiclesByEntranceNumberToExport.filter = this.vehiclesByEntranceNumber.filter;
    }
    this.reportService.findOperationsByEntranceNumberToExport(
      parkingId,
      vehiclesByEntranceNumberToExport
    ).subscribe(
      data => {
        callback(data)
      },
      error => {
        if (error) {
          this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
        }
      }
    )
  }

  exportOperationsByEntranceNumberToPdf() {
    this.getOperationsByEntranceNumberToExport(this.parkingId, operations => {
      const title = "Número de Entradas";
      const additionalInformation = "";
      const sourceData = operations;
      const data = [];
      data.push([
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tipo de Vehículo",
          bold: true
        },
        {
          text: "Cantidad",
          bold: true
        }
      ]);

      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(Utils.setVehicleType(sourceRow.vehicleType));
        dataRow.push(sourceRow.count);
        data.push(dataRow);
      });

      const docDefinition = {
        pageOrientation: "",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    });
  }

  exportOperationsByEntranceNumberToXls() {
    this.getOperationsByEntranceNumberToExport(this.parkingId, operations => {
      const title = "Número de Entradas";
      const columns = [
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "vehicleType",
          title: "Tipo de Vehículo"
        },
        {
          columnid: "count",
          title: "Cantidad"
        }
      ];
      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, operations]);
    });
  }
  // Vehicles by parking time
  getOperationsByParkingTime() {
    const entryByParkingTime = {
      currentPage: this.entryByParkingTime.currentPage,
      itemsPerPage: this.entryByParkingTime.itemsPerPage,
      from: this.entryByParkingTime.from,
      to: this.entryByParkingTime.to,
      gte: Utils.minutesToSeconds(this.entryByParkingTime.gte),
      sort: this.entryByParkingTime.sort,
      order: this.entryByParkingTime.order,
      filter: this.entryByParkingTime.filter
    }
    entryByParkingTime.currentPage = this.currentPageEntryByParkingTime - 1;
    this.reportService.findOperationsByParkingTime(this.parkingId, entryByParkingTime).subscribe(
      data => {
        this.operationsByParkingTime = data;
      }, error => {
        if (error) {
          this.toastService.show(error.message, { classname: 'bg-warning text-light' });
        }
      }
    )
  }

  getOperationsByParkingTimeToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const entryByParkingTimeToExport = {
      from: this.entryByParkingTime.from,
      to: this.entryByParkingTime.to,
      gte: Utils.minutesToSeconds(this.entryByParkingTime.gte),
      sort: this.entryByParkingTime.sort,
      order: this.entryByParkingTime.order,
      filter: ''
    };

    if (this.entryByParkingTime.filter.length >= 3) {
      entryByParkingTimeToExport.filter = this.entryByParkingTime.filter;
    }

    this.reportService.findOperationsByParkingTimeToExport(
      parkingId,
      entryByParkingTimeToExport
    ).subscribe(
      data => {
        callback(data)
      },
      error => {
        if (error) {
          this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
        }
      }
    )
  }

  exportOperationsByParkingTimeToPdf() {
    this.getOperationsByParkingTimeToExport(this.parkingId, operations => {
      const title = "Tiempo de Parqueo";
      const additionalInformation = "";
      const sourceData = operations;
      const data = [];
      data.push([
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tipo de Vehículo",
          bold: true
        },
        {
          text: "Duración",
          bold: true
        }
      ]);

      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(Utils.setVehicleType(sourceRow.vehicleType));
        dataRow.push(sourceRow.parkingTime);
        data.push(dataRow);
      });

      const docDefinition = {
        pageOrientation: "",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    });
  };

  exportOperationsByParkingTimeToXls() {
    this.getOperationsByParkingTimeToExport(this.parkingId, operations => {
      const title = "Tiempo de Parqueo";
      const columns = [
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "vehicleType",
          title: "Tipo de Vehículo"
        },
        {
          columnid: "parkingTime",
          title: "Duración"
        }
      ];
      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, operations]);
    });
  };

  // Vehicle entrance by plate
  getOperationsByVehiclePlate() {
    this.entryByVehiclePlate.currentPage = this.currentPageEntryByVehiclePlate - 1;
    this.reportService.findOperationByVehiclePlate(this.parkingId, this.entryByVehiclePlate).subscribe(
      (data: OperationsByVehiclePlate) => {
        this.operationsByVehiclePlate = data
      },
      error => {
        if (error) {
          this.toastService.show(error, { classname: 'bg-danger text-light' });
        }
      }
    )
  }

  getOperationsByVehiclePlateToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const entryByVehiclePlate = {
      from: this.entryByVehiclePlate.from,
      to: this.entryByVehiclePlate.to,
      vehiclePlate: this.entryByVehiclePlate.vehiclePlate,
      sort: this.entryByVehiclePlate.sort,
      order: this.entryByVehiclePlate.order,
      filter: ''
    }

    if (this.entryByVehiclePlate.filter.length >= 3) {
      entryByVehiclePlate.filter = this.entryByVehiclePlate.filter
    }

    this.reportService.findOperationByVehiclePlateToExport(
      parkingId,
      entryByVehiclePlate
    ).subscribe(
      data => {
        callback(data)
      },
      error => {
        if (error) {
          this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
        }
      }
    )
  }

  exportOperationsByVehiclePlateToPdf() {
    this.getOperationsByVehiclePlateToExport(this.parkingId, operations => {
      const title = "Tiempo de Parqueo";
      const additionalInformation = "";
      const sourceData = operations;
      const data = [];
      data.push([
        {
          text: " Fecha de Ingreso",
          bold: true
        },
        {
          text: "Duración",
          bold: true
        }
      ]);

      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.entrance);
        dataRow.push(sourceRow.parkingTime);
        data.push(dataRow);
      });

      const docDefinition = {
        pageOrientation: "",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    });
  };

  exportOperationsByVehiclePlateToXls() {
    this.getOperationsByVehiclePlateToExport(this.parkingId, operations => {
      const title = "Tiempo de Parqueo";
      const columns = [
        {
          columnid: "entrance",
          title: " Fecha de Ingreso"
        },
        {
          columnid: "parkingTime",
          title: "Duración"
        }
      ];
      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, operations]);
    });
  };


}
