import { Component, OnInit } from '@angular/core';
import { ReportService } from 'app/service/report.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { SubscriptionReport } from 'app/domain/subscriptionReport';
import { isString } from 'util';
import { CurrencyPipe } from '@angular/common';
import alasql from 'alasql';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

export type SortDirection = 1 | -1;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
}

declare interface SubscriptionsReport {
  totalCollected: number;
  totalItems: number;
  subscriptions: SubscriptionReport[]
}

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss']
})
export class SubscriptionsComponent implements OnInit {
  public subscriptionReport: SubscriptionsReport;
  public showProcessingIcon: boolean;
  public showInformationNotFound: boolean;
  public searchParameters: SearchParameters;
  public parkingId: string;
  public lastSort: SortEvent;
  public filterText: string;
  public filterSelected: string;
  initialTime = { hour: 0, minute: 0 };
  finalTime = { hour: 23, minute: 59 };
  currentPage = 1;
  itemsPerPage = 10;

  constructor(
    private reportService: ReportService,
    private toastService: ToastService,
    private currencyPipe: CurrencyPipe
  ) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.showProcessingIcon = false;
    this.showInformationNotFound = false;
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { startDate: new Date(), endDate: new Date() };
    this.searchParameters.startDate.setHours(0, 0, 0, 0);
    this.searchParameters.endDate.setHours(23, 59, 59, 999);
    this.lastSort = { column: 'created', direction: -1 }
    this.filterText = '';
    this.filterSelected = 'Todos';
    this.subscriptionReport = { subscriptions: [], totalCollected: 0, totalItems: 0 }
    this.findSubscriptionsReport();
  }

  onDateSelect(date, type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      this.searchParameters.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  onFilter(event) {
    if (this.filterText.length >= 3) {
      if (event.key === 'Enter') {

        this.findSubscriptionsReport()
      }
    }
  }

  onSort(column) {
    if (column === this.lastSort.column) {
      switch (this.lastSort.direction) {
        case 1:
          this.lastSort.direction = -1;
          break;
        case -1:
          this.lastSort.direction = 1;
          break;
      }
    } else {
      this.lastSort.column = column;
      this.lastSort.direction = 1;
    }
    this.findSubscriptionsReport();
  }

  exportSubscriptionsToPdf() {
    this.getSubscriptionsReportToExport(this.parkingId, response => {
      const title = "Informe de Suscripciones";
      const sourceData = response;
      const additionalInformation = `Total:${this.currencyPipe.transform(this.subscriptionReport.totalCollected, 'COP', '$')}`;
      const data = [];
      data.push([
        {
          text: "Fecha",
          bold: true
        },
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tarjeta",
          bold: true
        },
        {
          text: "Desde",
          bold: true
        },
        {
          text: "Hasta",
          bold: true
        },
        {
          text: "Tarifa",
          bold: true
        },
        {
          text: "Cantidad",
          bold: true
        },
        {
          text: "Subtotal",
          bold: true
        },
        {
          text: "IVA",
          bold: true
        },
        {
          text: "Total",
          bold: true
        },
        {
          text: "Usuario",
          bold: true
        }
      ]);
      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.created);
        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(sourceRow.additionalCode);
        dataRow.push(sourceRow.start);
        dataRow.push(sourceRow.end);
        dataRow.push(sourceRow.subscriptionPriceName);
        dataRow.push(sourceRow.quantity);
        dataRow.push(sourceRow.amount);
        dataRow.push(sourceRow.tax);
        dataRow.push(sourceRow.total);
        dataRow.push(sourceRow.userName);
        data.push(dataRow);
      });
      const docDefinition = {
        pageOrientation: "landscape",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    })
  }

  exportSubscriptionsToXls() {
    this.getSubscriptionsReportToExport(this.parkingId, response => {
      const title = "Informe de Suscripciones";
      const columns = [
        {
          columnid: "created",
          title: "Fecha"
        },
        {
          columnid: "vehiclePlate",
          title: "Matrícula/Código"
        },
        {
          columnid: "additionalCode",
          title: "Tarjeta"
        },
        {
          columnid: "start",
          title: "Desde"
        },
        {
          columnid: "end",
          title: "Hasta"
        },
        {
          columnid: "subscriptionPriceName",
          title: "Tarifa"
        },
        {
          columnid: "quantity",
          title: "Cantidad"
        },
        {
          columnid: "invoiceNumber",
          title: "Factura"
        },
        {
          columnid: "amount",
          title: "Subtotal"
        },
        {
          columnid: "tax",
          title: "IVA"
        },
        {
          columnid: "total",
          title: "Total"
        },
        {
          columnid: "userName",
          title: "Usuario"
        }
      ];
      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, response]);
    })

  }

  getReport(parkingId, data) {
    this.reportService.findSubscriptions(parkingId, data).subscribe(data => {

      if (this.filterSelected === 'Activos') {
        let dateNow = new Date();
        dateNow.setHours(0, 0, 0, 0);
        data.subscriptions = data.subscriptions.filter(subscription =>
          new Date(subscription.end) >= dateNow && subscription.state === 'ENABLED'
        )
      } else if (this.filterSelected === 'Inactivos') {
        let dateNow = new Date();
        data.subscriptions = data.subscriptions.filter(subscription =>
          new Date(subscription.end) < dateNow
        )
      }

      let totalCollected = 0;
      data.subscriptions.forEach(elem => {
        totalCollected = totalCollected + elem.total;
      });
      data.totalCollected = totalCollected;

      this.subscriptionReport = data;
      this.showProcessingIcon = false;
    }, error => {
      if (isString(error.error)) {
        this.showProcessingIcon = false;
        this.filterText = '';
        this.toastService.show(error.error, { classname: 'bg-warning text-light' });
      }
    })
  }

  findSubscriptionsReport() {
    const data = {
      currentPage: this.currentPage - 1,
      itemsPerPage: this.itemsPerPage,
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction,
      filter: ''
    }

    if (this.filterText.length >= 3) {
      data.filter = this.filterText;
    }

    this.getReport(this.parkingId, data)
  }

  getSubscriptionsReportToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const dataToExport = {
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction
    };

    if (this.filterText.length >= 3) {
      Object.assign(dataToExport, { filter: this.filterText });
    }

    this.reportService.findSubscriptionsToExport(parkingId, dataToExport).subscribe(
      data => {
        let totalCollected = 0;
        data.forEach(elem => {
          totalCollected = totalCollected + elem.total;
        });
        Object.assign(data, { totalCollected });
        callback(data);
      },
      error => {
        if (error.error !== null) {
          this.toastService.show(error.error, { classname: 'bg-warning text-light' });
        }
      }
    );
  };

  onChangeTime() {

  }

}
