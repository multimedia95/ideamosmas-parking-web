import { Component, OnInit } from '@angular/core';
import { StatisticsService } from 'app/service/statistics.service';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import * as Utils from '../../../assets/utils/utils';
import { DatePipe } from '@angular/common';
import { UserService } from 'app/service/user.service';

declare interface VehiclesParkedByType {
  total: number;
  result: [];
}

declare interface VehiclesQuantity {
  total: number;
  movid: number;
}

declare interface VehicleCount {
  count: number;
  vehicleType: string;
}

declare interface PaymentCount {
  total: number;
  paymentType: string;
  paymentName: string;
}

declare interface PaymentByDate {
  incomings: number;
  payments: number;
  date: string;
}

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
  days: number;
}

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  public companyId: string;
  public parkingId: string;
  public searchParameters: SearchParameters;
  public searchParametersByPeriod: SearchParameters;
  public vehiclesParkedByType: VehiclesParkedByType;
  public vehiclesQuantityToday: VehiclesQuantity[];
  public vehiclesPaymentsByDateRangeList: PaymentByDate[];
  public vehiclesQuantityLastWeek: VehiclesQuantity[];
  public totalCollectedToday: { total: number };
  public totalCollectedLastWeek: { total: number };
  public subscriptionStatistic: { total: number, count: number };
  public vehicleTypesQuantities: VehicleCount[];
  public paymentTypesQuantities: PaymentCount[];
  public vehiclesPaymentsByRangeList: PaymentByDate[];
  public daysDifference: number = 0;
  public vehiclesImages: { name: string, path: string }[] = [
    { name: 'CAR', path: '../../../assets/img/car.png' },
    { name: 'MOTORCYCLE', path: '../../../assets/img/motorcycle.png' },
    { name: 'BICYCLE', path: '../../../assets/img/bicycle.png' },
    { name: 'TRUCK', path: '../../../assets/img/truck.png' },
    { name: 'PUBLIC', path: '../../../assets/img/public.png' }
  ]
  public paymentImages: { name: string, path: string }[] = [
    { name: 'NORMAL', path: '../../../assets/img/normal.jpg' },
    { name: 'AGREEMENT', path: '../../../assets/img/agreement.jpg' },
    { name: 'SUBSCRIPTION', path: '../../../assets/img/subscription.jpg' }
  ]

  constructor(
    private statiscticsService: StatisticsService,
    private userService: UserService
  ) { }

  ngOnInit() {
    const from = new Date();
    from.setDate(from.getDate() - 7)
    const to = new Date();
    to.setDate(to.getDate() - 1)
    const startDate1 = new Date();
    startDate1.setDate(from.getDate() - 14)
    const startDate2 = new Date();
    startDate2.setDate(to.getDate() - 7)
    this.companyId = sessionStorage.getItem('companyId');
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { startDate: from, endDate: to, days: 0 };
    this.searchParametersByPeriod = { startDate: startDate1, endDate: startDate2, days: 7 };
    this.vehiclesParkedByType = { total: 0, result: [] };
    this.vehiclesQuantityToday = [{ total: 0, movid: 0 }];
    this.vehiclesQuantityLastWeek = [{ total: 0, movid: 0 }];
    this.totalCollectedLastWeek = { total: 0 };
    this.totalCollectedToday = { total: 0 };
    this.subscriptionStatistic = { total: 0, count: 0 };
    this.vehicleTypesQuantities = [];
    this.paymentTypesQuantities = [];
    this.vehiclesPaymentsByDateRangeList = [];
    this.vehiclesPaymentsByRangeList = [];
    this.getStatistic();
    this.getCharts();
    this.getCompareCharts();
    this.userService.finishLoading.subscribe(() => {
      this.companyId = sessionStorage.getItem('companyId');
      this.parkingId = sessionStorage.getItem('parkingId');
      this.getStatistic();
      this.getCharts();
      this.getCompareCharts();
    })
  }

  // Piechart vehicles
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: number[];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [];

  // Piechart payments
  public pieChartPaymentsOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartPaymentsLabels: Label[];
  public pieChartPaymentsData: number[];
  public pieChartPaymentsType: ChartType = 'pie';
  public pieChartPaymentsLegend = true;
  public pieChartPaymentsColors = [];

  // Linechart
  public lineChartDataVehicles: ChartDataSets[] = [
    { data: [] }
  ];
  public lineChartDataPayments: ChartDataSets[] = [
    { data: [] }
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptionsVehicles: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            display: true,
            labelString: 'Cantidad de Vehículos'
          }
        }
      ]
    },
    elements: {
      line: {
        tension: 0,
        borderWidth: 1
      }
    },
    annotation: {

    }
  };
  public lineChartOptionsPayments: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        scaleLabel: {
          display: true,
          labelString: 'Total Recaudado'
        },
        ticks: {
          beginAtZero: true,
          callback: function (value, index, values) {
            return '$ ' + Intl.NumberFormat().format(value);
          }
        }
      }]
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return ' $ ' + Intl.NumberFormat().format(+tooltipItem.yLabel);
        }
      }
    },
    elements: {
      line: {
        tension: 0,
        borderWidth: 1
      }
    },
    annotation: {

    }
  };
  public lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartType = 'line';

  // Linechart Compare
  public lineChartDataVehiclesCompare: ChartDataSets[] = [
    { data: [], label: 'Período 1' },
    { data: [], label: 'Período 2' }
  ];
  public lineChartDataPaymentsCompare: ChartDataSets[] = [
    { data: [], label: 'Período 1' },
    { data: [], label: 'Período 2' }
  ];
  public lineChartLabelsCompare: Label[] = [];
  public lineChartOptionsVehiclesCompare: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{
        display: true,
        position: 'top',
        id: 'x-axis-1',
        ticks: {
          callback: function (value, index, values) {
            return value
          }
        }
      },
      {
        display: true,
        position: 'bottom',
        id: 'x-axis-2',
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            display: true,
            labelString: 'Cantidad de Vehículos'
          }
        }
      ]
    },
    elements: {
      line: {
        tension: 0,
        borderWidth: 1
      }
    },
    annotation: {

    }
  };
  public lineChartOptionsPaymentsCompare: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{
        display: true,
        position: 'top',
        id: 'x-axis-1'
      },
      {
        display: true,
        position: 'bottom',
        id: 'x-axis-2',
      }],
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        scaleLabel: {
          display: true,
          labelString: 'Total Recaudado'
        },
        ticks: {
          beginAtZero: true,
          callback: function (value, index, values) {
            return '$ ' + Intl.NumberFormat().format(value);
          }
        }
      }]
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return ' $ ' + Intl.NumberFormat().format(+tooltipItem.yLabel);
        }
      }
    },
    elements: {
      line: {
        tension: 0,
        borderWidth: 1
      }
    },
    annotation: {

    }
  };
  public lineChartColorsCompare: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  onDateSelect(date, type, searchParameter) {
    if (type === 'initial') {
      searchParameter.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      searchParameter.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  getStatistic() {
    const from = new Date();
    from.setHours(0, 0, 0);
    console.log('from ' + from);

    const to = new Date();
    console.log('to ' + to);


    const lastWeekFrom = new Date();
    lastWeekFrom.setDate(lastWeekFrom.getDate() - 7);
    lastWeekFrom.setHours(0, 0, 0);
    console.log('lastWeekFrom ' + lastWeekFrom);


    const lastWeekTo = new Date();
    lastWeekTo.setDate(lastWeekTo.getDate() - 7);
    console.log('lastWeekTo ' + lastWeekTo);


    const vehicleTypes = [];
    const vehicleCount = [];
    const paymentTypes = [];
    const paymentNames = [];
    const paymentTotal = [];

    this.statiscticsService
      .findVehiclesParkedByType(this.parkingId, from, to)
      .subscribe(data => {
        this.vehiclesParkedByType = data;
      });

    this.statiscticsService
      .findVehiclesQuantityByDate(this.parkingId, from, to)
      .subscribe(data => {
        this.vehiclesQuantityToday = data;
      });

    this.statiscticsService
      .findVehiclesQuantityByDate(this.parkingId, lastWeekFrom, lastWeekTo)
      .subscribe(data => {
        this.vehiclesQuantityLastWeek = data;
      });

    this.statiscticsService
      .findTotalPaymentsByDate(this.parkingId, from, to)
      .subscribe(data => {
        this.totalCollectedToday = data;
      });

    this.statiscticsService
      .findTotalPaymentsByDate(this.parkingId, lastWeekFrom, lastWeekTo)
      .subscribe(data => {
        this.totalCollectedLastWeek = data;
      });

    this.statiscticsService
      .findTotalSubscriptionsByDate(this.parkingId, from, to)
      .subscribe(data => {
        this.subscriptionStatistic = data;
      });

    this.statiscticsService
      .findQuantityByVehicleTypes(this.parkingId, from, to)
      .subscribe(data => {
        this.vehicleTypesQuantities = data;
        this.vehicleTypesQuantities.forEach(elem => {
          vehicleTypes.push(Utils.setVehicleType(elem.vehicleType));
          vehicleCount.push(elem.count);
        });
        this.pieChartColors.push(this.setPieChartColor(vehicleTypes));
        this.pieChartLabels = vehicleTypes;
        this.pieChartData = vehicleCount;
      });

    this.statiscticsService
      .findPaymentsByPaymentTypes(this.parkingId, from, to)
      .subscribe(data => {
        this.paymentTypesQuantities = data;
        this.paymentTypesQuantities.forEach(elem => {
          if (elem.total > 0) {
            paymentTypes.push(Utils.setPaymentType(elem.paymentType));
            paymentNames.push(elem.paymentName);
            paymentTotal.push(elem.total);
          }
        });
        this.pieChartPaymentsColors.push(this.setPieChartColor(paymentTypes));
        this.pieChartPaymentsLabels = paymentTypes;
        this.pieChartPaymentsData = paymentTotal;
      });
  } //getStatistic()

  getCharts() {
    var from = this.searchParameters.startDate;
    var to = this.searchParameters.endDate;
    const dates = [];
    const numberOfVehicle = [];
    const totalOfPayment = [];

    this.statiscticsService
      .findVehiclesPaymentsGroupedByDate(this.parkingId, from, to)
      .subscribe(response => {
        this.vehiclesPaymentsByDateRangeList = response

        this.vehiclesPaymentsByDateRangeList.forEach(elem => {
          dates.push(elem.date);
          numberOfVehicle.push(elem.incomings);
          totalOfPayment.push(elem.payments);
        });
      });
    this.lineChartLabels = dates;
    this.lineChartDataVehicles[0].data = numberOfVehicle;
    this.lineChartDataPayments[0].data = totalOfPayment;
  }

  getCompareCharts() {
    var fromS1 = this.searchParametersByPeriod.startDate;
    var fromS2 = this.searchParametersByPeriod.endDate;
    var days = this.searchParametersByPeriod.days;

    const datesS1 = [];
    const numberOfVehicleS1 = [];
    const totalOfPaymentS1 = [];

    const datesS2 = [];
    const numberOfVehicleS2 = [];
    const totalOfPaymentS2 = [];

    const numberOfVehicles = [];
    const totalOfPayments = [];

    this.statiscticsService
      .findVehiclesPaymentsGroupedByRange(this.parkingId, fromS1, days)
      .subscribe(response => {
        this.vehiclesPaymentsByRangeList = response;

        this.vehiclesPaymentsByRangeList.forEach((elem, i) => {
          datesS1.push(elem.date);
          numberOfVehicleS1.push(elem.incomings);
          totalOfPaymentS1.push(elem.payments);
        });

        numberOfVehicles.push(numberOfVehicleS1);
        totalOfPayments.push(totalOfPaymentS1);
      });

    this.statiscticsService
      .findVehiclesPaymentsGroupedByRange(this.parkingId, fromS2, days)
      .subscribe(response => {
        this.vehiclesPaymentsByRangeList = response;

        this.vehiclesPaymentsByRangeList.forEach(elem => {
          datesS2.push(elem.date);
          numberOfVehicleS2.push(elem.incomings);
          totalOfPaymentS2.push(elem.payments);
        });

        numberOfVehicles.push(numberOfVehicleS2);
        totalOfPayments.push(totalOfPaymentS2);

        if (datesS1.length > 0 && datesS2.length > 0) {
          this.daysDifference = Utils.daysBetween(new Date(datesS1[0]), new Date(datesS2[0]))
        }

        this.lineChartLabelsCompare = datesS2;
        this.lineChartDataVehiclesCompare[0].data = numberOfVehicleS1;
        this.lineChartDataVehiclesCompare[1].data = numberOfVehicleS2;
        this.lineChartDataPaymentsCompare[0].data = totalOfPaymentS1;
        this.lineChartDataPaymentsCompare[1].data = totalOfPaymentS2;
      });
  }

  setPieChartColor(types) {
    const backgroundColor = [];
    types.forEach(type => {
      switch (type) {
        case "Carro":
          backgroundColor.push("rgba(206, 74, 44, 0.7)");
          break;

        case "Moto":
          backgroundColor.push("rgba(252, 195, 64, 0.7)");
          break;

        case "Bicicleta":
          backgroundColor.push("rgba(113, 190, 218, 0.7)");
          break;

        case "Camión":
          backgroundColor.push("rgba(7, 104, 67, 0.7)");
          break;

        case "Servicio Público":
          backgroundColor.push("rgba(48, 83, 122, 0.7)");
          break;

        case "Suscripción":
          backgroundColor.push("rgba(206, 74, 44, 0.7)");
          break;

        case "Normal":
          backgroundColor.push("rgba(113, 190, 218, 0.7)");
          break;

        default: //case "Convenio":
          backgroundColor.push("rgba(252, 195, 64, 0.7)");
          break;
      }
    });

    return { backgroundColor: backgroundColor };
  }

  setVehicleType(vehicleType) {
    return Utils.setVehicleType(vehicleType);
  }

  setPaymentType(paymentType) {
    return Utils.setPaymentType(paymentType);
  }

  setVehicleImage(vehicleType) {
    return this.vehiclesImages.find(v => v.name === vehicleType).path
  }

  setPaymentImage(paymentType) {
    return this.paymentImages.find(p => p.name === paymentType).path;
  }

}
