import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { ReportService } from 'app/service/report.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { isString } from 'util';
import { Payment } from 'app/domain/payment';
import alasql from 'alasql';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';


export type SortDirection = 1 | -1;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

declare interface FinalcialReport {
  totalCollected: number;
  totalItems: number;
  payments: Payment[]
}

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-financial',
  templateUrl: './financial.component.html',
  styleUrls: ['./financial.component.scss']
})
export class FinancialComponent implements OnInit {
  public financialReport: FinalcialReport;
  public showProcessingIcon: boolean;
  public showInformationNotFound: boolean;
  public searchParameters: SearchParameters;
  public parkingId: string;
  public lastSort: SortEvent;
  public filterText: string;
  initialTime = { hour: 0, minute: 0 };
  finalTime = { hour: 23, minute: 59 };
  currentPage = 1;
  itemsPerPage = 10;

  constructor(
    private reportService: ReportService,
    private toastService: ToastService,
    private currencyPipe: CurrencyPipe
  ) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { startDate: new Date(), endDate: new Date() };
    this.searchParameters.startDate.setHours(0, 0, 0, 0);
    this.searchParameters.endDate.setHours(23, 59, 59, 999);
    this.lastSort = { column: 'created', direction: -1 };
    this.filterText = '';
    this.financialReport = { payments: [], totalCollected: 0, totalItems: 0 };
    this.findFinancialReport();
  }

  onDateSelect(date, type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      this.searchParameters.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  onFilter(event) {
    if (this.filterText.length >= 3) {
      if (event.key === 'Enter') {
        this.findFinancialReport()
      }
    }
  }

  onSort(column) {
    if (column === this.lastSort.column) {
      switch (this.lastSort.direction) {
        case 1:
          this.lastSort.direction = -1;
          break;
        case -1:
          this.lastSort.direction = 1;
          break;
      }
    } else {
      this.lastSort.column = column;
      this.lastSort.direction = 1;
    }
    this.findFinancialReport();
  }

  exportFinancialReportToPdf() {
    this.findFinancialReportToExport(this.parkingId, reports => {
      const title = "Informe de Pagos";
      const additionalInformation = `Total:${this.currencyPipe.transform(this.financialReport.totalCollected, 'COP', '$')}`;
      const sourceData = reports;
      const data = [];
      data.push([
        {
          text: "Fecha",
          bold: true
        },
        {
          text: "No. Factura",
          bold: true
        },
        {
          text: "Usuario",
          bold: true
        },
        {
          text: "Tipo de Vehiculo",
          bold: true
        },
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tiempo",
          bold: true
        },
        {
          text: "Tarifa",
          bold: true
        },
        {
          text: "Subtotal",
          bold: true
        },
        {
          text: "IVA",
          bold: true
        },
        {
          text: "Total",
          bold: true
        }
      ]);
      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.created);
        dataRow.push(sourceRow.invoiceNumber);
        dataRow.push(sourceRow.userName);
        dataRow.push(sourceRow.vehicleType);
        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(sourceRow.parkingTime);
        dataRow.push(sourceRow.serviceName);
        dataRow.push(sourceRow.amount);
        dataRow.push(sourceRow.tax);
        dataRow.push(sourceRow.roundedTotal);
        data.push(dataRow);
      });
      const docDefinition = {
        pageOrientation: "landscape",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    })
  }

  exportFinancialReportToXls() {
    this.findFinancialReportToExport(this.parkingId, reports => {
      const title = "Informe de Pagos";
      const columns = [
        {
          columnid: "created",
          title: "Fecha"
        },
        {
          columnid: "invoiceNumber",
          title: "No. Factura"
        },

        {
          columnid: "userName",
          title: "Usuario"
        },
        {
          columnid: "vehicleType",
          title: "Tipo de Vehículo"
        },
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "parkingTime",
          title: "Tiempo"
        },
        {
          columnid: "serviceName",
          title: "Tarifa"
        },
        {
          columnid: "amount",
          title: "Subtotal"
        },
        {
          columnid: "tax",
          title: "IVA"
        },
        {
          columnid: "roundedTotal",
          title: "Total"
        }
      ];

      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, reports]);
    })
  }

  getReport(parkingId, data) {
    this.reportService.findFinancialReport(parkingId, data).subscribe(data => {
      this.financialReport = data;
      this.showProcessingIcon = false;
    }, error => {
      if (isString(error.error)) {
        this.showProcessingIcon = false;
        this.filterText = '';
        this.toastService.show(error.error, { classname: 'bg-warning text-light' });
      }
    })
  }

  findFinancialReport() {
    // this.showProcessingIcon = true;
    // this.financialReport = { payments: [], totalCollected: 0, totalItems: 0 };
    const data = {
      currentPage: this.currentPage - 1,
      itemsPerPage: this.itemsPerPage,
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction,
      filter: ''
    }

    if (this.filterText.length >= 3) {
      data.filter = this.filterText;
    }

    this.getReport(this.parkingId, data)
  }

  findFinancialReportToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const dataToExport = {
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction,
      filter: ''
    };
    if (this.filterText.length >= 3) {
      dataToExport
        .filter = this.filterText;
    }
    this.reportService.findFinancialReportToExport(
      parkingId,
      dataToExport
    ).subscribe(data => {
      callback(data)
    }, error => {
      this.toastService.show(error.error, { classname: 'bg-warning text-light' });
    })
  }

}
