import { Component, OnInit } from '@angular/core';

import { ReportService } from 'app/service/report.service';
import { ToastService } from 'app/service/toast-service/toast-service';
import { Agreement } from 'app/domain/agreement';
import { isString } from 'util';
import alasql from 'alasql';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as Utils from '../../../assets/utils/utils';

export type SortDirection = 1 | -1;

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

declare interface AgreementListItem {
  id: string;
  name: string;
  description: string;
  state: string;
}

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
  agreement: string;
}

declare interface Agreements {
  totalItems: number;
  totalAgrementTime: string;
  agreementPayments: Agreement[]
}

@Component({
  selector: 'app-agreements',
  templateUrl: './agreements.component.html',
  styleUrls: ['./agreements.component.scss']
})
export class AgreementsComponent implements OnInit {

  public agreementDropdown: AgreementListItem[];
  public agreements: Agreements;
  public showProcessingIcon: boolean;
  public showInformationNotFound: boolean;
  public searchParameters: SearchParameters;
  public parkingId: string;
  public lastSort: SortEvent;
  public filterText: string;
  initialTime = { hour: 0, minute: 0 };
  finalTime = { hour: 23, minute: 59 };
  currentPage = 1;
  itemsPerPage = 10;

  constructor(public reportService: ReportService, public toastService: ToastService) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.agreements = { agreementPayments: [], totalItems: 0, totalAgrementTime: '' };
    this.getAgreements();
    this.showProcessingIcon = false;
    this.showInformationNotFound = false;
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { agreement: '', startDate: new Date(), endDate: new Date() };
    this.searchParameters.startDate.setHours(0, 0, 0, 0);
    this.searchParameters.endDate.setHours(23, 59, 59, 999);
    this.searchParameters.agreement = 'Seleccione su opción';
    this.lastSort = { column: 'created', direction: -1 }
    this.filterText = '';
  }

  onChangeTime(type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setHours(this.initialTime.hour, this.initialTime.minute);
      console.log(this.searchParameters.startDate);
    } else if (type === 'final') {
      this.searchParameters.endDate.setHours(this.finalTime.hour, this.finalTime.minute);
      console.log(this.searchParameters.endDate);
    }
  }

  onDateSelect(date, type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      this.searchParameters.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  onFilter(event) {
    if (this.filterText.length >= 3) {
      if (event.key === 'Enter') {

        this.findAgreementsReport()
      }
    }
  }

  exportAgreementsToXls() {
    this.getAgremeentsReportToExport(this.parkingId, agreements => {
      const title = "Informe de Convenios";
      const columns = [
        {
          columnid: "operationId",
          title: "No. Operación"
        },
        {
          columnid: "created",
          title: "Fecha"
        },
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "vehicleType",
          title: "Tipo de Vehículo"
        },
        {
          columnid: "serviceName",
          title: "Tarifa"
        },
        {
          columnid: "parkingTime",
          title: "Tiempo de Parqueo"
        },
        {
          columnid: "serviceCourtesyTime",
          title: "Tiempo de Gracia"
        }
      ];

      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, agreements]);

    })
  }

  exportAgreementsToPdf() {
    this.getAgremeentsReportToExport(this.parkingId, agreements => {
      const title = "Informe de Convenios";
      const sourceData = agreements;
      const additionalInformation = "";
      const data = [];
      data.push([
        {
          text: "Fecha",
          bold: true
        },
        {
          text: "Matrícula",
          bold: true
        },
        {
          text: "Tipo de Vehículo",
          bold: true
        },
        {
          text: "Tarifa",
          bold: true
        },
        {
          text: "Tiempo de Parqueo",
          bold: true
        },
        {
          text: "Tiempo de Gracia",
          bold: true
        }
      ]);
      sourceData.forEach(sourceRow => {
        const dataRow = [];

        dataRow.push(sourceRow.created);
        dataRow.push(sourceRow.vehiclePlate);
        dataRow.push(Utils.setVehicleType(sourceRow.vehicleType));
        dataRow.push(sourceRow.serviceName);
        dataRow.push(sourceRow.parkingTime);
        dataRow.push(sourceRow.serviceCourtesyTime);
        data.push(dataRow);
      });

      const docDefinition = {
        pageOrientation: "",
        footer: {
          text: "www.ideamosmas.com",
          alignment: "center"
        },
        content: [
          {
            text: title,
            style: "header"
          },
          {
            table: {
              body: data
            }
          },
          {
            text: additionalInformation,
            style: "header"
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: "center",
            margin: [5, 2, 10, 20]
          }
        }
      };
      pdfMake.createPdf(docDefinition).download(`${title}.pdf`);
      pdfMake.createPdf(docDefinition).open();
    })
  }


  onSort(column) {
    if (column === this.lastSort.column) {
      switch (this.lastSort.direction) {
        case 1:
          this.lastSort.direction = -1;
          break;
        case -1:
          this.lastSort.direction = 1;
          break;
      }
    } else {
      this.lastSort.column = column;
      this.lastSort.direction = 1;
    }

    this.findAgreementsReport();
  }

  getAgreements() {
    this.reportService.findAgreements(sessionStorage.getItem("companyId")).subscribe(data => {
      this.agreementDropdown = data.agreements;
    }, error => {
      this.toastService.show("No se encontraron convenios configurados.", { classname: 'bg-warning text-light' });
    })
  }

  getReport(parkingId, agreementId, data) {
    this.reportService.findAgreementsReport(parkingId, agreementId, data).subscribe(data => {
      this.agreements = data;
      this.showProcessingIcon = false;
    }, error => {
      this.showProcessingIcon = false;
      this.filterText = '';
      if (isString(error.error)) {
        this.toastService.show(error.error, { classname: 'bg-warning text-light' });
      }
    })
  }

  findAgreementsReport() {
    // this.showProcessingIcon = true;
    // this.agreements = { agreementPayments: [], totalItems: 0, totalAgrementTime: '' };
    const data = {
      currentPage: this.currentPage - 1,
      itemsPerPage: this.itemsPerPage,
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction,
      filter: ''
    }

    if (this.filterText.length >= 3) {
      data.filter = this.filterText;
    }

    this.getReport(this.parkingId, this.searchParameters.agreement, data)
  }

  getAgremeentsReportToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-primary text-light' });
    const data = {
      currentPage: this.currentPage - 1,
      itemsPerPage: this.itemsPerPage,
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: this.lastSort.column,
      order: this.lastSort.direction,
      filter: ''
    }
    if (this.filterText.length >= 3) {
      data.filter = this.filterText;
    }
    this.reportService.findAgreementsReportToExport(
      parkingId,
      this.searchParameters.agreement,
      data
    ).subscribe(data => {
      callback(data)
    }, error => {
      this.toastService.show(error.error, { classname: 'bg-warning text-light' });
    })
  }

}
