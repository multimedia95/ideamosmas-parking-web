import { Component, OnInit } from '@angular/core';
import { ReportService } from 'app/service/report.service';
import { isString } from 'util';
import { ToastService } from 'app/service/toast-service/toast-service';
import { SalesSummary } from 'app/domain/salesSummary';
import alasql from 'alasql';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { SubscriptionReport } from 'app/domain/subscriptionReport';
import { PaymentService } from 'app/service/payment.service';

declare interface SearchParameters {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-daily-voucher',
  templateUrl: './daily-voucher.component.html',
  styleUrls: ['./daily-voucher.component.scss']
})
export class DailyVoucherComponent implements OnInit {
  public showProcessingIcon: boolean;
  public searchParameters: SearchParameters;
  public parkingId: string;
  public companyId: string;
  public salesSummary: SalesSummary;
  public cashBalance: number;
  public totalExpenses: number;
  public creditNotes: any;
  public totalCollected: number;
  public subscriptions: SubscriptionReport[];
  initialTime = { hour: 0, minute: 0 };
  finalTime = { hour: 23, minute: 59 };

  constructor(
    private reportService: ReportService,
    private paymentService: PaymentService,
    private toastService: ToastService,
    private currencyPipe: CurrencyPipe,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.companyId = sessionStorage.getItem('companyId');
    this.parkingId = sessionStorage.getItem('parkingId');
    this.searchParameters = { startDate: new Date(), endDate: new Date() };
    this.searchParameters.startDate.setHours(0, 0, 0, 0);
    this.searchParameters.endDate.setHours(23, 59, 59, 999);
    this.cashBalance = 0;
    this.totalExpenses = 0;
  }

  onDateSelect(date, type) {
    if (type === 'initial') {
      this.searchParameters.startDate.setFullYear(date.year, (date.month - 1), date.day);
    } else if (type === 'final') {
      this.searchParameters.endDate.setFullYear(date.year, (date.month - 1), date.day);
    }
  }

  doPdf() {
    const cashBalance = this.cashBalance;
    const expenses = this.totalExpenses;
    let totalInvoice = 0;

    if (this.salesSummary.invoiceRangeTo != 0 && this.salesSummary.invoiceRangeTo != null) {
      totalInvoice =
        this.salesSummary.invoiceRangeTo -
        this.salesSummary.invoiceRangeFrom +
        1;
    }

    let totalCreditNotes = 0;

    if (this.salesSummary.creditNoteRangeTo != 0) {
      totalCreditNotes =
        this.salesSummary.creditNoteRangeTo -
        this.salesSummary.creditNoteRangeFrom +
        1;
    }
    const docDefinition = {
      footer: {
        text: 'www.ideamosmas.com',
        alignment: 'center'
      },
      content: [
        {
          text: this.salesSummary.parkingName,
          style: 'header'
        },
        {
          text: 'REGIMEN COMUN',
          style: 'header'
        },
        {
          text: `NIT. ${this.salesSummary.taxIdentification}`,
          style: 'header'
        },
        {
          text: `Número de Resolución: ${
            this.salesSummary.taxResolutionNumber
            } de ${this.salesSummary.taxResolutionDate}`,
          style: 'header'
        },
        {
          text: `Prefijo ${
            null != this.salesSummary.taxResolutionRangePrefix ? this.salesSummary.taxResolutionRangePrefix : ''
            } Rango desde ${this.salesSummary.taxResolutionRangeFrom} hasta ${
            this.salesSummary.taxResolutionRangeTo
            }`,
          style: 'header'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: '*** COMPROBANTE INFORME DIARIO ***',
          style: 'header'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: `Fecha: ${this.salesSummary.date}`,
          style: 'left'
        },
        {
          text: `Responsable:  ${this.salesSummary.userName}`,
          style: 'left'
        },
        {
          text: `Hora Inicial:  ${this.salesSummary.startTime.slice(0, 8)}`,
          style: 'left'
        },
        {
          text: `Hora Final:  ${this.salesSummary.endTime.slice(0, 8)}`,
          style: 'left'
        },
        {
          text: `Rango de Facturas: ${
            null != this.salesSummary.invoiceRangeFrom ? this.salesSummary.invoiceRangeFrom : ''
            } - ${null != this.salesSummary.invoiceRangeTo ? this.salesSummary.invoiceRangeTo : ''}`,
          style: 'left'
        },
        {
          columns: [
            {
              width: '40%',
              text: `Rango de Notas Crédito: ${
                this.salesSummary.creditNoteRangeFrom
                } - ${this.salesSummary.creditNoteRangeTo}`,
              style: 'left'
            },
            {
              width: '60%',
              text: `No. De Transacciones: ventas: ${totalInvoice}, Notas credito: ${totalCreditNotes}`,
              style: 'left'
            }
          ]
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: 'TOTALES CONSOLIDADOS',
          style: 'header'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: `Saldo en Caja: ${this.currencyPipe.transform(cashBalance, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `(+) Ingresos Gravados: ${this.currencyPipe.transform(this.salesSummary.totalTaxedSales, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `(+) Ingresos No Gravados: ${this.currencyPipe.transform(this.salesSummary.totalDontTaxedSales, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `(-) Devoluciones Gravadas: ${this.currencyPipe.transform(this.salesSummary.taxedCreditNotes, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `(-) Devoluciones No Gravadas: ${this.currencyPipe.transform(this.salesSummary.dontTaxedCreditNotes, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `  (=) Ingresos Netos: ${this.currencyPipe.transform(this.salesSummary.netSales, 'COP', '$')}`,
          style: 'left_bold'
        },
        {
          text: `(+) IVA 19%: ${this.currencyPipe.transform(this.salesSummary.tax, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `  (=) Total Ingresos: ${this.currencyPipe.transform(this.salesSummary.total, 'COP', '$')}`,
          style: 'left_bold'
        },
        {
          text: `(-) Total Egresos: ${this.currencyPipe.transform(expenses, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `  Total En Caja: ${this.currencyPipe.transform(this.salesSummary.total - expenses, 'COP', '$')}`,
          style: 'left_bold'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: `  Medios de Pago:`,
          style: 'left_bold'
        },
        {
          text: `Efectivo: ${this.currencyPipe.transform(this.salesSummary.total, 'COP', '$')}`,
          style: 'left'
        },
        {
          text: `Venta Credito: $0`,
          style: 'left'
        },
        {
          text: `Tarjeta Credito: $0`,
          style: 'left'
        },
        {
          text: `Tarjeta Debito: $0`,
          style: 'left'
        },
        {
          text: `Bono: $0`,
          style: 'left'
        },
        {
          text: `Vales: $0`,
          style: 'left'
        },
        {
          text: `  Total Medios de Pago: ${this.currencyPipe.transform(this.salesSummary.total, 'COP', '$')}`,
          style: 'left_bold'
        },
        {
          text: '',
          style: 'center'
        },
        {
          text: `Ubicación: ${this.salesSummary.parkingAddress}`,
          style: 'header'
        }
      ],
      styles: {
        header: {
          bold: true,
          alignment: 'center',
          margin: [5, 2, 2, 5]
        },
        center: {
          alignment: 'center',
          margin: [5, 2, 2, 5]
        },
        left: {
          alignment: 'left',
          margin: [5, 2, 2, 5]
        },
        right: {
          alignment: 'right',
          margin: [5, 2, 2, 5]
        },
        left_bold: {
          alignment: 'left',
          margin: [5, 2, 2, 5],
          bold: true
        }
      }
    };

    pdfMake.createPdf(docDefinition).open();
    pdfMake
      .createPdf(docDefinition)
      .download('Comprobante informe diario' + '.pdf');

    this.exportToXls();
  }

  onSearch() {
    if (this.verifyMaximumRangeDate()) {
      const data = {
        from: this.searchParameters.startDate,
        to: this.searchParameters.endDate,
        creator: '',
        cashBoxBalance: 0,
        expenses: 0
      };
      this.showProcessingIcon = true;
      this.reportService.findDailyVoucher(
        this.companyId,
        this.parkingId,
        data
      ).subscribe(
        data => {
          this.showProcessingIcon = false;
          this.salesSummary = data;
          this.salesSummary.date = `${this.datePipe.transform(this.searchParameters.startDate, 'y-MM-dd HH:mm')} - ${this.datePipe.transform(this.searchParameters.endDate, 'y-MM-dd HH:mm')}`
        }, error => {
          if (isString(error.error)) {
            this.showProcessingIcon = false;
            this.toastService.show(error.error, { classname: 'bg-warning text-light' });
          }
        }
      )
    }
  }

  async getCreditNotes(from, to) {
    this.paymentService.findCreditNotes(this.parkingId, new Date(from).toJSON(), new Date(to).toJSON()).subscribe(
      data => {
        this.creditNotes = data.notes;
      },
      error => {
        this.toastService.show(error.error.message, { classname: 'bg-warning text-light' });
      }
    );
  }

  async getSubscriptions(parkingId, from, to) {
    const dataToExport = {
      from: from,
      to: to,
      sort: 'invoiceNumber',
      order: 1
    };

    this.reportService.findSubscriptionsToExport(parkingId, dataToExport).subscribe(
      data => {
        this.subscriptions = data;
      },
      error => {
        this.toastService.show(error.error.message, { classname: 'bg-warning text-light' });
      }
    );
  };

  async getFinancialReportToExport(parkingId, callback) {
    this.toastService.show("Descargando...", { classname: 'bg-warning text-light' });
    const dataToExport = {
      from: this.searchParameters.startDate,
      to: this.searchParameters.endDate,
      sort: 'invoiceNumber',
      order: 1
    };

    await this.getCreditNotes(dataToExport.from, dataToExport.to);
    await this.getSubscriptions(parkingId, dataToExport.from, dataToExport.to);

    this.reportService.findFinancialReportToExport(parkingId, dataToExport).subscribe(
      data => {
        callback(data);
      },
      error => {
        if (error !== null) {
          this.toastService.show(error.error.message, { classname: 'bg-warning text-light' });
        }
      }
    );
  };

  exportToXls() {
    this.getFinancialReportToExport(this.parkingId, response => {

      response = response.filter(item => item.roundedTotal > 0);

      const dataToDisplay = response.map(item => {
        item.created = item.created.slice(0, 10);
        return item;
      })

      if (this.subscriptions && this.subscriptions.length > 0) {
        dataToDisplay.push({
          vehicleType: "Suscripciones",
          vehiclePlate: "Suscripciones",
          created: "Suscripciones",
          serviceName: "Suscripciones",
          additionalCode: "Suscripciones",
          invoiceNumber: "Suscripciones",
          amount: 0,
          tax: 0,
          roundedTotal: 0,
          userName: "Suscripciones",
          parkingTimeInSeconds: 0,
          parkingTime: "Suscripciones",
        });

        this.subscriptions.forEach(subscription => {
          dataToDisplay.push({
            vehicleType: "-",
            vehiclePlate: subscription.additionalCode ?
              subscription.vehiclePlate + "/" + subscription.additionalCode.slice(subscription.additionalCode.length - 4, subscription.additionalCode.length - 1)
              : subscription.vehiclePlate,
            created: subscription.created.slice(0, 10),
            serviceName: subscription.subscriptionPriceName,
            additionalCode: subscription.additionalCode,
            invoiceNumber: subscription.invoiceNumber,
            amount: subscription.amount,
            tax: subscription.tax,
            roundedTotal: subscription.total,
            userName: "-",
            parkingTimeInSeconds: 0,
            parkingTime: "-",
          })
        });
      }


      if (this.creditNotes && this.creditNotes.length > 0) {
        dataToDisplay.push({
          vehicleType: "Notas Credito",
          vehiclePlate: "Notas Credito",
          created: "Notas Credito",
          serviceName: "Notas Credito",
          additionalCode: "Notas Credito",
          invoiceNumber: "Notas Credito",
          amount: 0,
          tax: 0,
          roundedTotal: 0,
          userName: "Notas Credito",
          parkingTimeInSeconds: 0,
          parkingTime: "Notas Credito",
        });

        this.creditNotes.forEach(creditNote => {
          dataToDisplay.push({
            vehicleType: "-",
            vehiclePlate: "-",
            created: creditNote.created.slice(0, 10),
            serviceName: "-",
            additionalCode: "-",
            invoiceNumber: 0,
            relatedBill: creditNote.rangePrefix + "-" + creditNote.invoiceNumber,
            amount: "-",
            tax: "-",
            roundedTotal: creditNote.total,
            userName: "-",
            parkingTimeInSeconds: 0,
            parkingTime: "-",
          })
        });
      }

      const title = "Informe de Pagos";
      const columns = [
        {
          columnid: "created",
          title: "Fecha"
        },
        {
          columnid: "invoiceNumber",
          title: "No. Factura"
        },
        {
          columnid: "relatedBill",
          title: "Afectada"
        },
        {
          columnid: "vehiclePlate",
          title: "Matrícula"
        },
        {
          columnid: "parkingTime",
          title: "Tiempo"
        },
        {
          columnid: "serviceName",
          title: "Tarifa"
        },
        {
          columnid: "amount",
          title: "Subtotal"
        },
        {
          columnid: "tax",
          title: "IVA"
        },
        {
          columnid: "roundedTotal",
          title: "Total"
        }
      ];

      const mystyle = {
        sheetid: title,
        headers: true,
        caption: {
          title
        },
        style: "background:#ffffff",
        column: {
          style: "font-size:20px"
        },
        columns,
        row: {
          style(sheet, row, rowidx) {
            return `background:${rowidx % 2 ? "white" : "white"}`;
          }
        },
        rows: {
          4: {
            cell: {
              style: "background:white"
            }
          }
        }
      };
      alasql(`SELECT * INTO XLS("${title}.xls",?) FROM ?`, [mystyle, dataToDisplay]);
    });
  };

  verifyMaximumRangeDate() {

    const from = this.searchParameters.startDate;
    const to = this.searchParameters.endDate;
    const maxDate = new Date(from);
    maxDate.setHours(maxDate.getHours() + 24);

    if (maxDate >= to) {
      return true;
    }

    this.toastService.show('El limite de tiempo es de 24 horas.', { classname: 'bg-warning text-light' });
  }

}
